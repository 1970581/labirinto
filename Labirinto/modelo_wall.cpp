﻿
#include "stdafx.h"
#include "global.h"
#include "iniciador.h"
#include <GL/glut.h>
#include "modelo_wall.h"
#include "logica.h"

//funcao que desenha o chao todo do labirinto.
void draw_floor(Mundo &mundo) {
	
	// Metodo para mostrar um chao de debug.
	if (mundo.opcoesDesenho.debugFloor) draw_debug_floor(mundo);

	// TODO desenhar um chao em condicoes.
	if (mundo.opcoesDesenho.realFloor)  draw_real_floor(mundo);

}


// Desenha um quadrado de chao para cada celula. Pare termos um chao de debug/basico.
void draw_debug_floor(Mundo &mundo) {
	GLfloat cell_largura_X = mundo.defaultCell.cell_largura_x;
	GLfloat cell_largura_Y = mundo.defaultCell.cell_largura_y;

	Pos cor; 
	cor.x = 0.5f; //RED
	cor.y = 0.5f; //GREEN
	cor.z = 0.5f; //BLUE

	Pos centro;	
	GLboolean alternarCor = GL_FALSE;
			
	int cell_X = 0;
	int cell_Y = 0;
	for (cell_X = 0; cell_X < mundo.map_cell_max_x; cell_X++) {
		for (cell_Y = 0; cell_Y < mundo.map_cell_max_y; cell_Y++) {

			getCentroDeCelula(cell_X, cell_Y, centro, mundo);
			draw_horisontal_square_from_center(centro, cell_largura_X, cell_largura_Y, cor);
			alternarCor = !alternarCor;
			if (alternarCor) cor.y += 0.05f;
			else cor.y -= 0.05f;
		}
	}

}

// Desenha o chao real final do labirinto.
void draw_real_floor(Mundo &mundo) {
	int cell_X = 0;
	int cell_Y = 0;

	// Iteramos todas as celulas do mapa.
	for (cell_X = 0; cell_X < mundo.map_cell_max_x; cell_X++) {
		for (cell_Y = 0; cell_Y < mundo.map_cell_max_y; cell_Y++) {
			draw_real_floor_at_cell(cell_X, cell_Y, mundo);

		}
	}

}

// Desenha um quadrado de chao real final  na celula X,Y.
void draw_real_floor_at_cell(int cell_X, int cell_Y, Mundo &mundo) {
	
	Pos posicaoDoCentroDaCelula;										// o centro da celula.
	getCentroDeCelula(cell_X, cell_Y, posicaoDoCentroDaCelula, mundo); // Obtemos o valor.

	GLfloat translacaoX = posicaoDoCentroDaCelula.x;
	GLfloat translacaoY = posicaoDoCentroDaCelula.y;
	GLfloat translacaoZ = 0.0f;  // Desenhamos no zero.

	// tamanho do quadrado de chao para o SCALE:
	GLfloat tamanho_x = mundo.defaultCell.cell_largura_x;
	GLfloat tamanho_y = mundo.defaultCell.cell_largura_y;






    // para alternarmos as cores e ficar como xadrez
	int alternarCor = (cell_X + cell_Y) % 2;   
	Pos cor;
	cor.x = 0.5f; //RED
	cor.y = 0.5f; //GREEN
	cor.z = 0.5f; //BLUE
	if (alternarCor) cor.y += 0.05f;
	else cor.y -= 0.05f;


	// A ------ B       Y               
	// |        |       ^      
	// |   .    |       |
	// |        |       #---> X
	// C ------ D


	glPushMatrix();
	{
		glColor3f(cor.x, cor.y, cor.z);		
		// RGB
		GLfloat corAmbiente[] = { cor.x, cor.y, cor.z, 1.0f };
		GLfloat corDiffuse[]  = { cor.x, cor.y, cor.z, 1.0f };
		GLfloat corSpecular[] = { cor.x, cor.y, cor.z, 1.0f };

		glMaterialfv(GL_FRONT, GL_AMBIENT,  corAmbiente);
		glMaterialfv(GL_FRONT, GL_DIFFUSE,  corDiffuse );
		glMaterialfv(GL_FRONT, GL_SPECULAR, corSpecular);

		glTranslatef(translacaoX, translacaoY, translacaoZ);		// deslocamos do centro do labirinto para o centro da celula
		glScalef(tamanho_x, tamanho_y, 0.0f);

		glBegin(GL_POLYGON);          // Poligono UNITARIO
		glNormal3f(  0.0f,  0.0f, 1.0f);	// Normal
		glVertex3f( -0.5f,  0.5f, 0.0f);   // Atencao a ordem. Esta em contraria aos ponteiros do relogio. Regra da mao direita.
		glVertex3f( -0.5f, -0.5f, 0.0f);
		glVertex3f(  0.5f, -0.5f, 0.0f);
		glVertex3f(  0.5f,  0.5f, 0.0f);
		glEnd();

		resetMaterialColorHugo();
	}
	glPopMatrix();

}



// WALL







// funcao que vai desenhar todas as paredes
void draw_allwalls(Mundo &mundo) {
	
	// desenha um quadrado em cada parede para debug de colisoes.
	if (mundo.opcoesDesenho.debugWalls) draw_debug_walls(mundo);

	// TODO desenhar paredes a 3D, solidas e nao solidas...
	if (mundo.opcoesDesenho.realWalls) draw_real_walls(mundo);

}

//Desenha as paredes reais do labirinto:
void draw_real_walls(Mundo &mundo) {
	int cell_X = 0;
	int cell_Y = 0;

	// Iteramos todas as celulas do mapa.
	for (cell_X = 0; cell_X < mundo.map_cell_max_x; cell_X++) {
		for (cell_Y = 0; cell_Y < mundo.map_cell_max_y; cell_Y++) {
			draw_wall_at_cell(cell_X, cell_Y, mundo);
		
		}
	}
}

// Desenha uma parede na celula X,Y.
void draw_wall_at_cell(int cell_X, int cell_Y, Mundo &mundo) {

	int solida = isCellSolid(cell_X, cell_Y, mundo); // So desenhamos paredes com colis�o, ou seja, solidas.

	// Obtemos o tipo de parede, pode ser util mais tarde se tivermos mais que 2 tipos de parede
	// e portanto desenharmos paredes diferentes conforme o tipo.
	GLint tipoParede = mundo.mapa.cell[cell_X][cell_Y];

	switch (tipoParede) {		//Switch, para podermos escolher qual a funcao de desenho consoante o tipo de parede.
		
		case WALL_SOLID:
			glBindTexture(GL_TEXTURE_2D, mundo.texturas.texturaWall);
			draw_wall_at_cell_as_WALL_SOLID(cell_X, cell_Y, mundo);
			glBindTexture(GL_TEXTURE_2D, NULL);
			break;

		case WALL_OBJECT_DOOR_1:
			if (!mundo.door.apanheiKey1) {   //Se ainda nao apanhei a chaves, desenho a porta.
				glBindTexture(GL_TEXTURE_2D, mundo.texturas.texturaDoor);
				draw_wall_at_cell_as_WALL_SOLID(cell_X, cell_Y, mundo);  //JOAO SUBSTITUI AQUI PELA TUA CHAMADA FUNCAO DE DESENHO DA PORTA OU DEIXA MESMO ISTO.
				glBindTexture(GL_TEXTURE_2D, NULL);
			}
			break;

		case WALL_EMPTY:
			break;
		default:
			;
	};


}

// Desenha uma parede do tipo WALL_SOLID na celula X Y
void draw_wall_at_cell_as_WALL_SOLID(int cell_X, int cell_Y, Mundo &mundo) {
	Pos posicaoDoCentroDaCelula;  // o centro da celula.

	getCentroDeCelula(cell_X, cell_Y, posicaoDoCentroDaCelula, mundo); // Obtemos o valor.

	GLfloat translacaoX = posicaoDoCentroDaCelula.x;
	GLfloat translacaoY = posicaoDoCentroDaCelula.y;
	GLfloat translacaoZ = 0.0f + mundo.defaultCell.cell_altura_z / 2;  // Temos de subir a pararede metade da sua altura.

	glPushMatrix();
	{
		glColor3f(1.0, 1.0, 1.0);	//Cor Branca
		//glColor3f(0.0, 0.0, 1.0);	//Cor Azul
		glTranslatef(translacaoX, translacaoY, translacaoZ);		// deslocamos para o centro da celula
		desenhaCubo(mundo.defaultCell.cell_largura_x / 2.0f, mundo.defaultCell.cell_largura_y / 2.0f, mundo.defaultCell.cell_altura_z / 2.0f);  // desenhamos o cubo ja com o tamanho da celula.
	}
	glPopMatrix();

}


// desenha paredes de debug para testes de colisao. Apenas um rectangulo flutuante.
void draw_debug_walls( Mundo &mundo) {  

	GLfloat cell_largura_X = mundo.defaultCell.cell_largura_x;
	GLfloat cell_largura_Y = mundo.defaultCell.cell_largura_y;
	GLfloat subir = 0.05f; // subimos as paredes um pouco acima do chao para nao ficarem bugadas.

	Pos cor;
	cor.x = 0.7f; //RED
	cor.y = 0.0f; //GREEN
	cor.z = 0.0f; //BLUE

	Pos centro;

	int cell_X = 0;
	int cell_Y = 0;
	for (cell_X = 0; cell_X < mundo.map_cell_max_x; cell_X++) {
		for (cell_Y = 0; cell_Y < mundo.map_cell_max_y; cell_Y++) {

			int solida = isCellSolid(cell_X, cell_Y, mundo); // So desenhamos paredes com colisao, ou seja, solidas.
			if (solida) {
				getCentroDeCelula(cell_X, cell_Y, centro, mundo);
				centro.z += subir;
				draw_horisontal_square_from_center(centro, cell_largura_X, cell_largura_Y, cor);
			}
		}
	}

}

// Desenha um quadrado a volta de um ponto central. Hugo, usado em Desenhar o chao de debug.
void draw_horisontal_square_from_center(Pos center, GLfloat comprimentoLadoX,GLfloat comprimentoLadoY, Pos cor) {

	GLfloat raioX = comprimentoLadoX / 2.0f;
	GLfloat raioY = comprimentoLadoY / 2.0f;

	// A ------ B       Y               o raio e metade da largura do quadrado...
	// |        |       ^      
	// |   .    |       |
	// |        |       #---> X
	// C ------ D

	Pos a, b, c, d;
	a.x = center.x - raioX;
	a.y = center.y + raioY;
	a.z = center.z;

	b.x = center.x + raioX;
	b.y = center.y + raioY;
	b.z = center.z;

	c.x = center.x - raioX;
	c.y = center.y - raioY;
	c.z = center.z;

	d.x = center.x + raioX;
	d.y = center.y - raioY;
	d.z = center.z;

	glColor3f(cor.x, cor.y, cor.z); //cor
	glBegin(GL_POLYGON);
	glVertex3f(a.x, a.y, a.z);   // Atencao a ordem. Esta em contraria aos ponteiros do relogio. Regra da mao direita.
	glVertex3f(c.x, c.y, c.z);
	glVertex3f(d.x, d.y, d.z);
	glVertex3f(b.x, b.y, b.z);
	glEnd();
}


// Desenhar um Poligno
// Funcao criada pelo Joao Original.
void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[])
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);

	glTexCoord2f(1.0, 0.0);				// Textura
	glVertex3fv(a);

	glTexCoord2f(0.0, 0.0);				// Textura
	glVertex3fv(b);

	glTexCoord2f(0.0, 1.0);				// Textura
	glVertex3fv(c);

	glTexCoord2f(1.0, 1.0);				// Textura
	glVertex3fv(d);
	glEnd();
}

//Desenha um Cubo
// Funcao criada pelo Joao Original.
void desenhaCubo(GLfloat x, GLfloat y, GLfloat z)
{
	GLfloat vertices[][3] = { { -x,-y,-z },
	{ x,-y,-z },
	{ x,y,-z },
	{ -x,y,-z },
	{ -x,-y,z },
	{ x,-y,z },
	{ x,y,z },
	{ -x,y,z } };
	GLfloat normais[][3] = { { 0,0,-z * 2 },{ 0,0,z * 2 },{ 0,-y * 2,0 },{ 0,y * 2,0 },{ -x * 2,0,0 },{ x * 2,0,0 }
		// acrescentar as outras normais...
	};


	// glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[3]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[4]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[5]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[1]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[2]);

	//glBindTexture(GL_TEXTURE_2D, NULL);
}




