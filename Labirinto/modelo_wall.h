#pragma once

#ifndef MODELO_WALL_H
#define MODELO_WALL_H

#include "global.h"

void draw_floor(Mundo & mundo);

void draw_debug_floor(Mundo & mundo);

void draw_real_floor(Mundo & mundo);

void draw_real_floor_at_cell(int cell_X, int cell_Y, Mundo & mundo);

void draw_allwalls(Mundo & mundo);

void draw_real_walls(Mundo & mundo);

void draw_wall_at_cell(int cell_X, int cell_Y, Mundo & mundo);

void draw_wall_at_cell_as_WALL_SOLID(int cell_X, int cell_Y, Mundo & mundo);

void draw_debug_walls( Mundo & mundo);

void draw_horisontal_square_from_center(Pos center, GLfloat comprimentoLadoX, GLfloat comprimentoLadoY, Pos cor);

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat d[], GLfloat normal[]);

void desenhaCubo(GLfloat x, GLfloat y, GLfloat z);

#endif


