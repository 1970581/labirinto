#pragma once





#ifndef LOGICA_H
#define LOGICA_H

#include "global.h"

void test(int x);

void copiarCreatura(Creatura original, Creatura & clone);

int tentarMoverCreaturaFrente(Creatura &creatura, Mundo & mundo);

int tentarMoverCreaturaTras(Creatura & creatura, Mundo & mundo);

int tentarRodarCreaturaEsquerda(Creatura &creatura, Mundo & mundo);

int tentarRodarCreaturaDireita(Creatura &creatura, Mundo & mundo);

void frontFromPlayer(Pos & pos, Creatura player);

void leftFromPlayer(Pos & pos, Creatura player);

void backFromPlayer(Pos & pos, Creatura player);

void rightFromPlayer(Pos & pos, Creatura player);

int isTypoParedeSolida(GLint typoParede, Mundo mundo);

int isCellSolid(int x, int y, Mundo mundo);

void qualACelulaDestePonto(Pos posicao, int & xCell, int & yCell, Mundo mundo);

void getCentroDeCelula(int cellX, int cellY, Pos & posicaoCentralDaCelula, Mundo mundo);

int isPosicaoEmColisaoBasica(Pos pos, Mundo mundo);

int isCreaturaEmColisaoBasica(Creatura creatura, Mundo mundo);

void resetMaterialColorHugo();

int is_jogador_na_saida(Mundo & mundo);

void avancarLevel(Mundo & mundo);

void changestatusbar(Mundo & mundo);

void logicaChaves(Mundo & mundo);



#endif


