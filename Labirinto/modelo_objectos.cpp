
#include "stdafx.h"
#include "global.h"
#include "iniciador.h"
#include <GL/glut.h>
#include "modelo_objectos.h"
#include "logica.h"


//Desenha os objectos que constam no mapa de paredes.
void draw_Wall_Objects(Mundo &mundo) {
	int cell_X = 0;
	int cell_Y = 0;

	// Iteramos todas as celulas do mapa.
	for (cell_X = 0; cell_X < mundo.map_cell_max_x; cell_X++) {
		for (cell_Y = 0; cell_Y < mundo.map_cell_max_y; cell_Y++) {

			GLint tipoParedeObjecto = mundo.mapa.cell[cell_X][cell_Y];

			switch (tipoParedeObjecto) {		//Switch, para podermos escolher qual a funcao de desenho consoante o tipo de parede/objecto.

			case WALL_OBJECT_EXIT:
				draw_Wall_Object_Exit_at_cell(cell_X, cell_Y, mundo);		// Saida.
				break;
			case WALL_OBJECT_KEY_1:
				if (mundo.door.grauDeApanhoDaChaves > 0)	draw_Wall_Object_Key_1_at_cell(cell_X, cell_Y, mundo);	// CHAVES 1.
				break;
			case WALL_OBJECT_DOOR_1:
				if (!mundo.door.abertaPorta1)	draw_Wall_Object_Door_1_at_cell(cell_X, cell_Y, mundo);	// CHAVES 1.
				break;

			case WALL_EMPTY:
				break;
			default:
				;
			};

		}
	}
}


void draw_Wall_Object_Key_1_at_cell(int cell_X, int cell_Y, Mundo &mundo) {

	Pos posicaoDoCentroDaCelula;  // o centro da celula.
	getCentroDeCelula(cell_X, cell_Y, posicaoDoCentroDaCelula, mundo); // Obtemos o valor.

	GLfloat translacaoX = posicaoDoCentroDaCelula.x;
	GLfloat translacaoY = posicaoDoCentroDaCelula.y;
	GLfloat translacaoZ = mundo.defaultCell.cell_altura_z / 1.0f;  // Desenhamos no centro da celula.

																   // tamanho do quadrado de chao para o SCALE:
	GLfloat tamanho_x = mundo.defaultCell.cell_largura_x;
	GLfloat tamanho_y = mundo.defaultCell.cell_largura_y;
	GLfloat tamanho_z = mundo.defaultCell.cell_altura_z;

	GLUquadric* quad = mundo.quadraticas.objecto[1];   //Key_1 Tubo
	

	Pos cor;
	cor.x = 0.8f; //RED
	cor.y = 0.8f; //GREEN
	cor.z = 0.1f; //BLUE

	GLfloat alpha = 0.5f;

	glPushMatrix(); {
		glColor3f(cor.x, cor.y, cor.z);
		// RGB
		GLfloat corAmbiente[] = { cor.x, cor.y, cor.z, alpha };
		GLfloat corDiffuse[] = { cor.x, cor.y, cor.z, alpha };
		GLfloat corSpecular[] = { 0.2f, 0.5f, 0.0f, alpha };

		glMaterialfv(GL_FRONT, GL_AMBIENT, corAmbiente);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, corDiffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, corSpecular);

		glTranslatef(translacaoX, translacaoY, translacaoZ);		// deslocamos do centro do labirinto para o centro da celula
																	//glScalef(tamanho_x, tamanho_y, tamanho_z);				// Desnecessarior visto termos ja um raio correcto.
		
		GLfloat anguloRotacao0 = mundo.door.rotacaoChaves * 360;	// Serve para a chaves estar sempre a rodar.
		glRotatef(anguloRotacao0, 0.0f, 0.0f, 1.0f);
		

		//Roda
		GLdouble raioExterior = mundo.defaultCell.cell_altura_z / 8.0;
		GLdouble raioInterior = raioExterior / 2.0;
		GLint nSides = 36;
		GLint nRings = 36;

		//Pe da chaves
		GLdouble base = raioExterior - raioInterior;
		GLdouble top = base;
		GLdouble height = raioExterior * 4.0;
		GLint slices = nSides;
		GLint stacks = nRings;



		//Rotacao em para ficar para cima.
		glRotatef(90.0f, 1.0, 0.0, 0.0);

		GLfloat reducao = mundo.door.grauDeApanhoDaChaves;
		reducao *= 1.0;// Teste.

		// Diminui o torus.
		glScalef( reducao, reducao, reducao);
			
		
		glutSolidTorus(raioInterior, raioExterior, nSides, nRings);

		//Rotacao em para ficar para cima.
		glRotatef(-90.0f, 1.0, 0.0, 0.0);
		glTranslatef(0.0, 0.0, -raioExterior * 5.0);

		gluCylinder(quad, base, top, height, slices, stacks);

		GLUquadric* quad2 = mundo.quadraticas.objecto[2];   //Key_1 Esfera achatada

		GLdouble raioEsfera = raioInterior *2;
		
		// Deslocar para o lado do tubo.
		glTranslatef(raioInterior * 2.0, 0.0, +raioExterior * 1.0);
		//Achatar a esfera distorcendo as cordenadas.
		glScalef(1.0, 0.25, 1.0);

		gluSphere(quad2, raioEsfera, slices, stacks);
		
		resetMaterialColorHugo();	// impede de as selecoes de cores glMaterial overflow para os outros objectos que se esqueceram de defenir glMaterial.

	}
	glPopMatrix();

}


void draw_Wall_Object_Door_1_at_cell(int cell_X, int cell_Y, Mundo &mundo) {

}


void desenhaPoligonoHugo(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[])
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}


void desenhaCuboHugo()
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };
	GLfloat normais[][3] = { { 0,0,-1 },
	{ 0,1,0 },
	{ -1,0,0 },
	{ 1,0,0 },
	{ 0,0,1 },
	{ 0,-1,0 }
		// acrescentar as outras normais...
	};


	// glBindTexture(GL_TEXTURE_2D, texID);
	// SEM TEXTURAS:
	desenhaPoligonoHugo(vertices[1],vertices[0],vertices[3],vertices[2],normais[0]);
	desenhaPoligonoHugo(vertices[2],vertices[3],vertices[7],vertices[6],normais[1]);
	desenhaPoligonoHugo(vertices[3],vertices[0],vertices[4],vertices[7],normais[2]);
	desenhaPoligonoHugo(vertices[6],vertices[5],vertices[1],vertices[2],normais[3]);
	desenhaPoligonoHugo(vertices[4],vertices[5],vertices[6],vertices[7],normais[4]);
	desenhaPoligonoHugo(vertices[5],vertices[4],vertices[0],vertices[1],normais[5]);
	

	//COM TEXTURAS:

	//GLfloat s0 = 0.0f;
	//GLfloat t0 = 0.0f;
	//GLfloat s = 0.25;
	//GLfloat t = 0.25;
	/*
	glBindTexture(GL_TEXTURE_2D, texID);
	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], s0, s, t0, t);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], s0, s, t0, t);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], s0, s, t0, t);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], s0, s, t0, t);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], s0, s, t0, t);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], s0, s, t0, t);
	glBindTexture(GL_TEXTURE_2D, NULL);
	*/
}






// Desenha o objecto da saida.
void draw_Wall_Object_Exit_at_cell(int cell_X, int cell_Y, Mundo &mundo){
	Pos posicaoDoCentroDaCelula;  // o centro da celula.
	getCentroDeCelula(cell_X, cell_Y, posicaoDoCentroDaCelula, mundo); // Obtemos o valor.

	GLfloat translacaoX = posicaoDoCentroDaCelula.x;
	GLfloat translacaoY = posicaoDoCentroDaCelula.y;
	GLfloat translacaoZ = mundo.defaultCell.cell_altura_z / 2.0f ;  // Desenhamos no centro da celula.

								 // tamanho do quadrado de chao para o SCALE:
	GLfloat tamanho_x = mundo.defaultCell.cell_largura_x;
	GLfloat tamanho_y = mundo.defaultCell.cell_largura_y;
	GLfloat tamanho_z = mundo.defaultCell.cell_altura_z;

	GLUquadric* quad = mundo.quadraticas.objecto[0];
	GLdouble radius = (mundo.defaultCell.cell_largura_x + mundo.defaultCell.cell_largura_y) / 4.0;
	radius /= 2.0;
	GLint slices = 360;
	GLint stacks = 36;

	Pos cor;
	cor.x = 0.0f; //RED
	cor.y = 0.3f; //GREEN
	cor.z = 0.8f; //BLUE

	GLfloat alpha = 0.5f;

	glPushMatrix(); {
		glColor3f(cor.x, cor.y, cor.z);
		// RGB
		GLfloat corAmbiente[] = { cor.x, cor.y, cor.z, alpha };
		GLfloat corDiffuse[]  = { cor.x, cor.y, cor.z, alpha };
		GLfloat corSpecular[] = { 0.2f, 0.5f, 0.0f, alpha };

		glMaterialfv(GL_FRONT, GL_AMBIENT, corAmbiente);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, corDiffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, corSpecular);

		glTranslatef(translacaoX, translacaoY, translacaoZ);		// deslocamos do centro do labirinto para o centro da celula
		//glScalef(tamanho_x, tamanho_y, tamanho_z);				// Desnecessarior visto termos ja um raio correcto.

	

		gluSphere(quad, radius, slices, stacks);

		resetMaterialColorHugo();	// impede de as selecoes de cores glMaterial overflow para os outros objectos que se esqueceram de defenir glMaterial.
	
	}
	glPopMatrix();
	
}



// Desenha o jogador no ecra.
void drawplayer(Mundo mundo) {

	// Se debug, vamos desenhar a area referencia de colisao
	if (mundo.opcoesDesenho.debugShowPlayerColisionArea) drawDebugColisionArea(mundo.mainplayer);
	if (mundo.opcoesDesenho.realPlayer) drawboneco(mundo.mainplayer, mundo);
	// TODO desenhar o jogador em condicoes.

}




// DEBUG: Desenha a area de referencia de colisao de um jogador.
void drawDebugColisionArea(Creatura player) {

	GLfloat altura = player.z + 0.1f;  // Subimos um pouco o local de desenho para se ver melhor.

	Pos front, left, right, back;	
	

	//Obtencao dos pontos com base no angulo de orientacao.
	frontFromPlayer(front, player);
	leftFromPlayer(left, player);
	backFromPlayer(back, player);
	rightFromPlayer(right, player);

	//Alevantar o Z um bocado para aparecer melhor.
	front.z = altura;
	left.z = altura;
	back.z = altura;
	right.z = altura;

	//Defenir cores
	Pos green; green.x = 0.0f, green.y = 1.0f, green.z = 0.0f;
	Pos blue; blue.x = 0.0f, blue.y = 0.0f, blue.z = 0.6f;

	// Desenhar o triangulo da "Area de colisao" do player
	draw_triangle(front, left, right, green);
	draw_triangle(left, back, right, blue);

}

// Desenha um triagulo com uma cor 
void draw_triangle(Pos a, Pos b, Pos c, Pos cor) {
	
	
		// VERTICES NO SENTIDO CONTRARIO AOS PONTEIROS DO RELOGIO PARA FACE FICAR PARA CIMA.	
	glColor3f(cor.x, cor.y, cor.z); //cor
	glBegin(GL_POLYGON);
	glVertex3f(a.x, a.y, a.z);
	glVertex3f(b.x, b.y, b.z);
	glVertex3f(c.x, c.y, c.z);
	glEnd();
}


void desenhaPoligonoBoneco(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat cor[], GLfloat normal[])
{

	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glColor3fv(cor);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}

void desenhaCuboBoneco(GLfloat r, GLfloat g, GLfloat b)
{
	GLfloat vertices[][3] = {
	{ -0.5,-0.5,-0.5 }, //0
	{ 0.5,-0.5,-0.5 },  //1
	{ 0.5,0.5,-0.5 },	//2
	{ -0.5,0.5,-0.5 },	//3
	{ -0.5,-0.5,0.5 },	//4
	{ 0.5,-0.5,0.5 },	//5
	{ 0.5,0.5,0.5 },	//6
	{ -0.5,0.5,0.5 }	//7

	};

	GLfloat cores[][3] = { { 0.0,1.0,1.0 },
	{ 1.0,0.0,0.0 },
	{ 1.0,1.0,0.0 },
	{ 0.0,1.0,0.0 },
	{ 1.0,0.0,1.0 },
	{ 0.0,0.0,1.0 },
	{ 1.0,1.0,1.0 } };

	GLfloat cor[3];
	cor[0] = r;
	cor[1] = g;
	cor[2] = b;

	GLfloat normais[][3] = { {0,0,-1},{0,1,0}, {-1,0,0},{1,0,0},{0,0,1},{0,-1,0} };

	desenhaPoligonoBoneco(vertices[1], vertices[0], vertices[3], vertices[2], cor, normais[0]);
	desenhaPoligonoBoneco(vertices[2], vertices[3], vertices[7], vertices[6], cor, normais[1]);
	desenhaPoligonoBoneco(vertices[3], vertices[0], vertices[4], vertices[7], cor, normais[2]);
	desenhaPoligonoBoneco(vertices[6], vertices[5], vertices[1], vertices[2], cor, normais[3]);
	desenhaPoligonoBoneco(vertices[4], vertices[5], vertices[6], vertices[7], cor, normais[4]);
	desenhaPoligonoBoneco(vertices[5], vertices[4], vertices[0], vertices[1], cor, normais[5]);
}


void drawboneco(Creatura player, Mundo &mundo) {

	// guardar os valores das variaveis globais
	GLfloat alturaBoneco = player.altura;
	GLfloat larguraBoneco = player.largura;
	GLfloat comprimentoBoneco = player.comprimento;

	//dimensoes cabeca
	GLfloat cabecaAlturaCentro = alturaBoneco * 5.5 / 6;
	GLfloat cabecaAltura = alturaBoneco / 6;
	GLfloat cabecaLargura = larguraBoneco / 6.0;
	GLfloat cabecaComprimento = comprimentoBoneco;

	//dimensoes tronco
	GLfloat troncoAlturaCentro = alturaBoneco * 4.0 / 6.0;
	GLfloat troncoAltura = alturaBoneco * 2.0 / 6.0;
	GLfloat troncoLargura = larguraBoneco * 4.0 / 6.0;
	GLfloat troncoComprimento = comprimentoBoneco;

	//dimensoes pernas
	GLfloat pernaAlturaCentro = alturaBoneco * 1.5 / 6.0;
	GLfloat pernaAltura = alturaBoneco * 3 / 6.0;
	GLfloat pernaLargura = larguraBoneco * 1.5 / 6.0;
	GLfloat pernaComprimento = comprimentoBoneco;

	//esquerda
	GLfloat pernaEsquerdaLarguraCentro = larguraBoneco * (-1.25 / 6.0);
	GLfloat anguloPernaEsquerdaRodar = 90.0f * mundo.mainplayer.indicadorMovimento;
	GLfloat anguloPernaEsquerda = -45.0f;
	//direita
	GLfloat pernaDireitaLarguraCentro = larguraBoneco * (1.25 / 6.0);
	GLfloat anguloPernaDireitaRodar = -90.0f * mundo.mainplayer.indicadorMovimento;
	GLfloat anguloPernaDireita = 45.0f;

	//dimensoes anteBraco
	GLfloat anteBracoAlturaCentro = alturaBoneco * 3.15 / 6.0;
	GLfloat anteBracoAltura = alturaBoneco * 1.3 / 6.0;
	GLfloat anteBracoLargura = larguraBoneco * 1.0 / 6.0;
	GLfloat anteBracoComprimento = comprimentoBoneco;

	//esquerdo
	GLfloat anteBracoEsquerdoLarguraCentro = larguraBoneco * -2.5 / 6.0;
	GLfloat anguloAnteBracoEsquerdoRodar = -180.0f * mundo.mainplayer.indicadorMovimento;
	GLfloat anguloAnteBracoEsquerdo = 90.0f;
	//direito
	GLfloat anteBracoDireitoLarguraCentro = larguraBoneco * 2.5 / 6.0;
	GLfloat anguloAnteBracoDireitoRodar = 180.0f * mundo.mainplayer.indicadorMovimento;
	GLfloat anguloAnteBracoDireito = -90.0f;

	//dimensoes braco
	GLfloat bracoAlturaCentro = alturaBoneco * 4.4 / 6.0;
	GLfloat bracoAltura = alturaBoneco * 1.2 / 6.0;
	GLfloat bracoLargura = larguraBoneco * 1.0 / 6.0;
	GLfloat bracoComprimento = comprimentoBoneco;
	//esquerdo
	GLfloat bracoEsquerdoLarguraCentro = larguraBoneco * -2.5 / 6.0;
	//direito
	GLfloat bracoDireitoLarguraCentro = larguraBoneco * 2.5 / 6.0;

	Pos cor;
	cor.x = 0.1f; //RED
	cor.y = 0.4f; //GREEN
	cor.z = 0.9f; //BLUE
	GLfloat alpha = 0.6f;

	glTranslatef(player.x, player.y, player.z);
	glRotatef(player.orientacao, 0.0, 0.0, 1.0);

	glPushMatrix(); {
		glColor3f(cor.x, cor.y, cor.z);
		GLfloat corAmbiente[] = { cor.x, cor.y, cor.z, alpha };
		GLfloat corDiffuse[] = { cor.x, cor.y, cor.z, alpha };
		GLfloat corSpecular[] = { 0.1f, 0.4f, 0.0f, alpha };
		glMaterialfv(GL_FRONT, GL_AMBIENT, corAmbiente);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, corDiffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, corSpecular);

		//cabeca
		glPushMatrix(); {
			//glNormal3f(0.0f, 0.0f, 1.0f);
			glTranslatef(0, 0, cabecaAlturaCentro);
			//glScalef(cabecaComprimento , cabecaLargura, cabecaAltura);

			//desenhaCuboBoneco(1.0,1.0,1.0);

			glutSolidSphere(cabecaAltura / 2, 100, 10);

		}

		glPopMatrix();

		//tronco
		glPushMatrix(); {
			glTranslatef(0, 0, troncoAlturaCentro);
			glScalef(troncoComprimento, troncoLargura, troncoAltura);
			desenhaCuboBoneco(0.0, 0.0, 1.0);
		}
		glPopMatrix();

		//perna esquerda
		glPushMatrix(); {
			glTranslatef(0, pernaEsquerdaLarguraCentro, pernaAlturaCentro + (pernaAltura / 2));
			glRotatef(anguloPernaEsquerda, 0, 1, 0);
			glRotatef(anguloPernaEsquerdaRodar, 0, 1, 0);
			glTranslatef(0, 0, (-pernaAltura / 2));

			glScalef(pernaComprimento, pernaLargura, pernaAltura);
			desenhaCuboBoneco(1.0, 0.0, 0.0);
		}
		glPopMatrix();

		//perna direita
		glPushMatrix(); {
			glTranslatef(0, pernaDireitaLarguraCentro, pernaAlturaCentro + (pernaAltura / 2));
			glRotatef(anguloPernaDireita, 0, 1, 0);
			glRotatef(anguloPernaDireitaRodar, 0, 1, 0);
			glTranslatef(0, 0, (-pernaAltura / 2));

			glScalef(pernaComprimento, pernaLargura, pernaAltura);
			desenhaCuboBoneco(1.0, 0.0, 0.0);
		}
		glPopMatrix();

		//braco esquerdo
		glPushMatrix(); {
			glTranslatef(0, bracoEsquerdoLarguraCentro, bracoAlturaCentro);
			glScalef(bracoComprimento, bracoLargura, bracoAltura);
			desenhaCuboBoneco(0.0, 1.0, 0.0);
		}
		glPopMatrix();

		//braco direito
		glPushMatrix(); {
			glTranslatef(0, bracoDireitoLarguraCentro, bracoAlturaCentro);
			glScalef(bracoComprimento, bracoLargura, bracoAltura);
			desenhaCuboBoneco(0.0, 1.0, 0.0);
		}
		glPopMatrix();

		//anteBraco esquerdo
		glPushMatrix(); {
			glTranslatef(0, anteBracoEsquerdoLarguraCentro, anteBracoAlturaCentro + (anteBracoAltura / 2));
			glRotatef(anguloAnteBracoEsquerdo, 0, 1, 0);
			glRotatef(anguloAnteBracoEsquerdoRodar, 0, 1, 0);
			glTranslatef(0, 0, (-anteBracoAltura / 2));
			glScalef(anteBracoComprimento, anteBracoLargura, anteBracoAltura);
			desenhaCuboBoneco(0.0, 0.0, 0.0);
		}
		glPopMatrix();

		//anteBraco direito
		glPushMatrix(); {
			glTranslatef(0, anteBracoDireitoLarguraCentro, anteBracoAlturaCentro + (anteBracoAltura / 2));
			glRotatef(anguloAnteBracoDireito, 0, 1, 0);
			glRotatef(anguloAnteBracoDireitoRodar, 0, 1, 0);
			glTranslatef(0, 0, (-anteBracoAltura / 2));
			glScalef(anteBracoComprimento, anteBracoLargura, anteBracoAltura);
			desenhaCuboBoneco(0.0, 0.0, 0.0);
		}
		glPopMatrix();
	}
	glPopMatrix();
}




