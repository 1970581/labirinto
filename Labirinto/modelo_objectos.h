#pragma once


#ifndef MODELO_OBJECTOS_H
#define MODELO_OBJECTOS_H

#include "global.h"




void draw_Wall_Objects(Mundo & mundo);

void draw_Wall_Object_Key_1_at_cell(int cell_X, int cell_Y, Mundo & mundo);

void draw_Wall_Object_Door_1_at_cell(int cell_X, int cell_Y, Mundo & mundo);

void desenhaPoligonoHugo(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat d[], GLfloat normal[]);

void desenhaCuboHugo();

void draw_Wall_Object_Exit_at_cell(int cell_X, int cell_Y, Mundo & mundo);

void drawplayer(Mundo mundo);

void drawDebugColisionArea(Creatura player);

void draw_triangle(Pos a, Pos b, Pos c, Pos cor);

void desenhaPoligonoBoneco(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat d[], GLfloat cor[], GLfloat normal[]);

void desenhaCuboBoneco(GLfloat r, GLfloat g, GLfloat b);

void drawboneco(Creatura player, Mundo & mundo);


#endif


