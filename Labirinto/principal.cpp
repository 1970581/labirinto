
#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <GL/glut.h>

#include "global.h"
#include "logica.h"
#include "iniciador.h"
#include "modelo_wall.h"
#include "modelo_objectos.h"
#include "principal.h"
#include "levelLoader.h"
#include "TextureLoader.h"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DEBUG               1

#define DELAY_MOVIMENTO     20
#define RAIO_ROTACAO        20


// VARIAVEIS GLOBAIS     (apenas neste file, usar extern XYZ para global a aplicacao)

Estado estado;
//Modelo modelo;

// As nossas definicoes de ESTROTURAS do labirinto estao no global.h 

Mundo mundo;



/* Inicializa��o do ambiente OPENGL */
void Init(void)
{

	srand((unsigned)time(NULL));

	mundo.pause = GL_FALSE;

	estado.debug = DEBUG;
	estado.menuActivo = GL_FALSE;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.camera.eye.x = 40;
	estado.camera.eye.y = 40;
	estado.camera.eye.z = 40;
	estado.camera.center.x = 0;
	estado.camera.center.y = 0;
	estado.camera.center.z = 0;
	estado.camera.up.x = 0;
	estado.camera.up.y = 0;
	estado.camera.up.z = 1;
	//estado.ortho = GL_TRUE;
	estado.ortho = GL_FALSE;
	estado.camera.fov = 60;

	estado.teclas.a = estado.teclas.q = estado.teclas.z = estado.teclas.x = \
		estado.teclas.up = estado.teclas.down = estado.teclas.left = estado.teclas.right = GL_FALSE;

	

	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	//glutIgnoreKeyRepeat(GL_TRUE);


	//TEXTURAS INICIO  ###########################################################
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	glEnable(GL_TEXTURE_2D);   // LIGAR TEXTURAS.
	
	//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_REPLACE);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);      //MODULATE para termos luz nas texturas. Senao fica tudo muito claro, porque ignora a iluminacao. 


	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	TextureLoader *pTextureLoader = new TextureLoader();			// Criar objecto do loader de texturas.
	pTextureLoader->SetAlphaMatch(FALSE, 0, 0xFF, 0xFF);			// ???, desactivei pk usamos JPGs.
	pTextureLoader->SetMipMapping(TRUE);							// Se usarmos mipmapping ja fica...
	pTextureLoader->SetTextureFilter(txNoFilter);					// Sem filtro.

	glTexture textura_parede;
	glTexture textura_porta;
	//pTextureLoader->LoadTextureFromDisk("Data\\Wall.tga", &m_MyWallTexture);
	
	pTextureLoader->LoadTextureFromDisk("wallhugo.jpg", &textura_parede);    //Carregamos as duas texturas.
	pTextureLoader->LoadTextureFromDisk("doorhugo.jpg", &textura_porta);
		
	mundo.texturas.texturaWall = textura_parede.TextureID;					// Guardamos o apontador das texturas na nossa extrotura.
	mundo.texturas.texturaDoor = textura_porta.TextureID;
		
	glBindTexture(GL_TEXTURE_2D, NULL);							// IMPORTANTE: LIMPAR O BUFFER DE TEXTURA, SENAO PINTA TUDO...


	//glBindTexture(GL_TEXTURE_2D, m_MyWallTexture.TextureID);
	//glDisable(GL_TEXTURE_2D);
	//pTextureLoader->FreeTexture(&m_MyWallTexture);


	// FIM DAS TEXTURAS.


	//INIT DO LABIRINTO

	iniciarMundo(mundo);	// iniciar os dados do mundo + jogador + paredes + mapa.
	load_all_levels(mundo); // Carrega os niveis, do ficheiro de texto.
	avancarLevel(mundo);	// Carregar o nivel 1, o nivel zero nao existe.

	mundo.vistas.vistaActiva = 2;		//Coloca como default view 1 pessoa.
	mundo.iluminacao.iluminacaoLigada = !mundo.iluminacao.iluminacaoLigada;	// Liga iluminacao
}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)width, (GLint)height);


	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// gluOrtho2D(left,right,bottom,top); 
	// projeccao ortogonal 2D, com profundidade (Z) entre -1 e 1
	if (estado.debug)
		printf("Reshape %s\n", (estado.ortho) ? "ORTHO" : "PERSPECTIVE");

	if (estado.ortho)
	{
		if (width < height)
			glOrtho(-20, 20, -20 * (GLdouble)height / width, 20 * (GLdouble)height / width, -100, 100);
		else
			glOrtho(-20 * (GLdouble)width / height, 20 * (GLdouble)width / height, -20, 20, -100, 100);
	}
	else
	{
		//gluPerspective(estado.camera.fov, (GLfloat)width / height, 1, 100);   // Antiga do prof.
		int vista = mundo.vistas.vistaActiva;
		
		GLfloat znear = mundo.mainplayer.comprimento / 2;  // Corte de objectos proximos. https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml
		gluPerspective(mundo.vistas.cameras[vista].fov, (GLfloat)width / height, znear, 100);   // Nova para as vistas.
		//Infelismente a cabeca do boneco e muito grande pelo que vamos ter de evitar desenhar o boneco na vista 02.

	}
		

	// Matriz Modelview
	// Matriz onde s�o realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}


void desenhaPoligonoAntigo(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat cor[])
{

	glBegin(GL_POLYGON);
	glColor3fv(cor);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}


void desenhaCuboAntigo()
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };

	GLfloat cores[][3] = { { 0.0,1.0,1.0 },
	{ 1.0,0.0,0.0 },
	{ 1.0,1.0,0.0 },
	{ 0.0,1.0,0.0 },
	{ 1.0,0.0,1.0 },
	{ 0.0,0.0,1.0 },
	{ 1.0,1.0,1.0 } };

	desenhaPoligonoAntigo(vertices[1], vertices[0], vertices[3], vertices[2], cores[0]);
	desenhaPoligonoAntigo(vertices[2], vertices[3], vertices[7], vertices[6], cores[1]);
	desenhaPoligonoAntigo(vertices[3], vertices[0], vertices[4], vertices[7], cores[2]);
	desenhaPoligonoAntigo(vertices[6], vertices[5], vertices[1], vertices[2], cores[3]);
	desenhaPoligonoAntigo(vertices[4], vertices[5], vertices[6], vertices[7], cores[4]);
	desenhaPoligonoAntigo(vertices[5], vertices[4], vertices[0], vertices[1], cores[5]);
}

void desenhaCuboAntigo(GLfloat r, GLfloat g, GLfloat b)
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };

	GLfloat cores[][3] = { { 0.0,1.0,1.0 },
	{ 1.0,0.0,0.0 },
	{ 1.0,1.0,0.0 },
	{ 0.0,1.0,0.0 },
	{ 1.0,0.0,1.0 },
	{ 0.0,0.0,1.0 },
	{ 1.0,1.0,1.0 } };

	GLfloat cor[3];
	cor[0] = r;
	cor[1] = g;
	cor[2] = b;

	desenhaPoligonoAntigo(vertices[1], vertices[0], vertices[3], vertices[2], cor);
	desenhaPoligonoAntigo(vertices[2], vertices[3], vertices[7], vertices[6], cor);
	desenhaPoligonoAntigo(vertices[3], vertices[0], vertices[4], vertices[7], cor);
	desenhaPoligonoAntigo(vertices[6], vertices[5], vertices[1], vertices[2], cor);
	desenhaPoligonoAntigo(vertices[4], vertices[5], vertices[6], vertices[7], cor);
	desenhaPoligonoAntigo(vertices[5], vertices[4], vertices[0], vertices[1], cor);
}



void strokeString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glColor3d(0.0, 0.0, 0.0);
	glTranslated(x, y, z);
	glScaled(s, s, s);
	for (i = 0;i<n;i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);

	glPopMatrix();

}

void bitmapString(char *str, double x, double y)
{
	int i, n;

	// fonte pode ser:
	// GLUT_BITMAP_8_BY_13
	// GLUT_BITMAP_9_BY_15
	// GLUT_BITMAP_TIMES_ROMAN_10
	// GLUT_BITMAP_TIMES_ROMAN_24
	// GLUT_BITMAP_HELVETICA_10
	// GLUT_BITMAP_HELVETICA_12
	// GLUT_BITMAP_HELVETICA_18
	//
	// int glutBitmapWidth  	(	void *font , int character);
	// devolve a largura de um car�cter
	//
	// int glutBitmapLength 	(	void *font , const unsigned char *string );
	// devolve a largura de uma string (soma da largura de todos os caracteres)

	n = strlen(str);
	glRasterPos2d(x, y);
	for (i = 0;i<n;i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void bitmapCenterString(char *str, double x, double y)
{
	int i, n;

	n = strlen(str);
	glRasterPos2d(x - glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (const unsigned char *)str)*0.5, y);
	for (i = 0;i<n;i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}


// ... definicao das rotinas auxiliares de desenho ...

void setLight0()
{
	Pos pos;
	pos.x = mundo.zero_x;
	pos.y = mundo.zero_y;
	pos.z = mundo.zero_z + (mundo.defaultCell.cell_altura_z * 10.0f);  // Luz alta.
	
	pos.x = 0.1f;
	pos.y = 0.1f;
	pos.z = 1.0f;

	GLfloat light_pos[4] = { pos.x, pos.y, pos.z, 0.0 };   // O quarto valor a 1.0 indica que a luz e para todas as direcoes.

														   //Luz branca total.
	GLfloat u = 0.65f;

	GLfloat light_ambient[]  = { 0.25f, 0.25f, 0.25f, 1.0f };   // Reflectida convem ser baixa intensidade ou fica tudo branco
	GLfloat light_diffuse[]  = { u, u, u, 1.0f };
	GLfloat light_specular[] = { u, u, u, 1.0f };



	// ligar e definir fonte de luz 2
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // Algo sobre o angulo de luz specular ser em relacao ao olho/camera http://www.manpagez.com/man/3/glLightModeli/
}

// Luz para vista de cima.
void setLight1()
{
	Pos pos;
	pos.x = mundo.vistas.cameras[1].eye.x;
	pos.y = mundo.vistas.cameras[1].eye.y;
	pos.z = mundo.vistas.cameras[1].eye.z;
	
	GLfloat light_pos[4] = { pos.x, pos.y, pos.z, 1.0 };   // O quarto valor a 1.0 indica que a luz e para todas as direcoes.
	
	GLfloat u = 0.65f;
	//Luz branca total.
	GLfloat light_ambient[]  = { 0.1f, 0.1f, 0.1f, 1.0f };   // Reflectida convem ser baixa intensidade ou fica tudo branco
	GLfloat light_diffuse[]  = { u, u, u, 1.0f };
	GLfloat light_specular[] = { u, u, u, 1.0f };



	// ligar e definir fonte de luz 2
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_pos);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // Algo sobre o angulo de luz specular ser em relacao ao olho/camera http://www.manpagez.com/man/3/glLightModeli/
}


// Luz especifica para a vista de primeira pessoa.
void setLight2()
{
	
	Pos pos;		
	pos.x = mundo.mainplayer.x;
	pos.y = mundo.mainplayer.y;
	pos.z = mundo.mainplayer.z + (mundo.mainplayer.altura * 0.5f);
	//pos.z /= 0.5;

	Pos frenteJogador;
	frontFromPlayer(frenteJogador, mundo.mainplayer);
	

	
	
	
	backFromPlayer(pos, mundo.mainplayer);

	Pos vectorDirecao;
	vectorDirecao.x = frenteJogador.x - pos.x; //0.0f; //mundo.vistas.cameras[2].center.x;
	vectorDirecao.y = frenteJogador.y - pos.y; //1.0f; //mundo.vistas.cameras[2].center.y;
	vectorDirecao.z = 0.0f; //mundo.vistas.cameras[2].center.z;

	GLfloat light_pos[4] = { pos.x, pos.y, pos.z, 1.0 };

	//Luz branca total.
	GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };   // Reflectida convem ser baixa intensidade ou fica tudo branco
	GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	GLfloat spotDirection[]  = { vectorDirecao.x, vectorDirecao.y, vectorDirecao.z };

	GLfloat aberturaDaLanterna = 17.0f;
	aberturaDaLanterna = 20.0f;		// Alterado, para o foco ser mais largo e termos luz junto a parede. Senao os cantos nao ficam iluminados e nao da luz.

	GLfloat atenuacao = 0.20f;
	atenuacao = 0.30f;

	// ligar e definir fonte de luz 2
	glEnable(GL_LIGHT2);
	glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT2, GL_POSITION, light_pos);

	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, spotDirection);		// Foco.    http://www.glprogramming.com/red/chapter05.html

	glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, aberturaDaLanterna);					// Angulo da luz da lanterna.
	//glLightf(GL_LIGHT2, GL_QUADRATIC_ATTENUATION, atenuacao);			// Reducao com a distancia.
	glLightf(GL_LIGHT2, GL_LINEAR_ATTENUATION, atenuacao);
	glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 0.5f);					// Reducao da luz com a distancia ao raio central.

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE); // Algo sobre o angulo de luz specular ser em relacao ao olho/camera http://www.manpagez.com/man/3/glLightModeli/
}


void processLight() {
	// ligar ilumina��o
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_LIGHT0);
	glDisable(GL_LIGHT1);
	glDisable(GL_LIGHT2);

	glPushMatrix();
	int view = mundo.vistas.vistaActiva;
	switch (view) {		//Switch, para podermos escolher qual a luz por tipo de vista.

	case 0:
		setLight0();
		break;
	case 1:
		setLight1();
		break;
	case 2:
		setLight2();
		break;
	default:
		;
	};
	glPopMatrix();
}


// Funcao de escrida dos SLIDES DO PROF.
void drawString(GLfloat x, GLfloat y, GLfloat z,
	GLfloat scale,
	char* msg)
{
	glPushMatrix();
	glTranslatef(x, y, z);
	glScalef(scale, scale, scale);
	for (int i = 0; i < strlen(msg); i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, msg[i]);
	glPopMatrix();
}



//FUNCAO DE ESCRITA.
void escrever() {

	char msg[256] = { "XIIIX aaaaaaaaaaaaaaaaaaa" };
	GLfloat mat_w[4] = { 0.5, 3.0, 0.5, 1.0 };

	glPushMatrix();
	glLoadIdentity();
	//gluLookAt(0, 0, 5, 0, 0, 0, 0, 1, 0);						//Original
	gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glColor4fv(mat_w);
	//glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_w);		//Original
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_w);
	//drawString(-2, -2, 0, 0.002, msg);
	drawString(-3.5, 2.5, 0, 0.002, mundo.texto.statusbar);

	resetMaterialColorHugo();
	glPopMatrix();

}



// Callback de desenho

void Draw(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
		

	// Posicionar camera conforme a vista activa.
	int vista = mundo.vistas.vistaActiva;
	gluLookAt(
		mundo.vistas.cameras[vista].eye.x,		mundo.vistas.cameras[vista].eye.y,		mundo.vistas.cameras[vista].eye.z,
		mundo.vistas.cameras[vista].center.x,	mundo.vistas.cameras[vista].center.y,	mundo.vistas.cameras[vista].center.z,
		mundo.vistas.cameras[vista].up.x,		mundo.vistas.cameras[vista].up.y,		mundo.vistas.cameras[vista].up.z
	);

	GLboolean luzActiva = mundo.iluminacao.iluminacaoLigada;
	if (luzActiva) processLight();
	else glDisable(GL_LIGHTING);

	
	
	//OUTDATED: servia para os testes da escrita.
	/*
	char str[250] = { "Ola" };
	double x = 1.0;
	double y = 1.0;
	bitmapString(str, x, y);
	bitmapCenterString(str, x, y);
	strokeString(str, x, y, 1.0, 1.0);
	*/

	// ... chamada das rotinas auxiliares de desenho ...
	//###################################
	//  LABIRINTO
	draw_floor(mundo);			//modelo_wall.ccp
	draw_allwalls(mundo);		//modelo_wall.ccp
	draw_Wall_Objects(mundo);	//modelo_objectos.cpp
	//Apenas desenhamos o boneco se nao tiver na vista de 1 pessoa.
	if (mundo.vistas.vistaActiva != 2) drawplayer(mundo);			//modelo_objectos.cpp


	//ESCRITA
	escrever();


	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}

// Faz o update da vista 1
void updateView1(Mundo &mundo) {
	
	// Vista de tras por cima.

	int vista = 1;
	GLfloat metrosFrentePlayer = 4.0f;
	
	Pos objecto;					// Obter uma posicao de objecto a olhar para a X metros a frente do player.
	
	Creatura clone;
	copiarCreatura(mundo.mainplayer, clone);
	clone.comprimento = metrosFrentePlayer * 2;
	frontFromPlayer(objecto, clone);


	Pos olho;						// Obter uma posicao de olho/camera X metros atras do player.
	backFromPlayer(olho, clone);
	olho.z = mundo.mainplayer.z + ((mundo.mainplayer.altura) * 2.0f);

	
	mundo.vistas.cameras[vista].center.x = objecto.x;	// Agora actualizamos a vista.
	mundo.vistas.cameras[vista].center.y = objecto.y;
	mundo.vistas.cameras[vista].center.z = objecto.z;

	mundo.vistas.cameras[vista].eye.x = olho.x;
	mundo.vistas.cameras[vista].eye.y = olho.y;
	mundo.vistas.cameras[vista].eye.z = olho.z;

	
}

//Faz o update da vista 2
void updateView2(Mundo &mundo) {
	
	int vista = 2;
	GLfloat metrosFrentePlayer = 0.1f;


	Pos objecto;					// Obter uma posicao de objecto a olhar para a X metros a frente do player.	
	Creatura clone;
	copiarCreatura(mundo.mainplayer, clone);
	clone.comprimento = metrosFrentePlayer * 2;
	frontFromPlayer(objecto, clone);
	objecto.z = mundo.mainplayer.z + (mundo.mainplayer.altura);   //altura da cabeca.

	Pos olho;						// Obter uma posicao de olho/camera X metros atras do player.
	// X metros atras do player.
	backFromPlayer(olho, clone);
	olho.z = mundo.mainplayer.z + (mundo.mainplayer.altura);	// altura da cabeca	

	mundo.vistas.cameras[vista].center.x = objecto.x;	// Agora actualizamos a vista.
	mundo.vistas.cameras[vista].center.y = objecto.y;
	mundo.vistas.cameras[vista].center.z = objecto.z;

	mundo.vistas.cameras[vista].eye.x = olho.x;
	mundo.vistas.cameras[vista].eye.y = olho.y;
	mundo.vistas.cameras[vista].eye.z = olho.z;
}



// Faz o update das vistas conforme a vista activa.
void updateView(Mundo &mundo) {

	int vista = mundo.vistas.vistaActiva;
	switch (vista) {		//Switch, para podermos fazer update aos dados da vista.

	case 1:
		updateView1(mundo);
		break;
	case 2:
		updateView2(mundo);
		break;
	default:
		;
	};
}




/*******************************
***   callbacks timer/idle   ***
*******************************/

void Timer(int value)
{
	glutTimerFunc(estado.delayMovimento, Timer, 0);
	// ... accoes do temporizador ... 

	if (estado.menuActivo || mundo.pause ) // sair em caso de o jogo estar parado ou menu estar activo
		return;
	
	
	//TECLAS DO LABIRINTO (hugo)
	int playerMoveu = 0;
	int playerMoveuFrenteTras = 0;
	if (mundo.teclas.moverFrente) playerMoveu = tentarMoverCreaturaFrente(mundo.mainplayer, mundo);
	if (mundo.teclas.moverTras) playerMoveu = tentarMoverCreaturaTras(mundo.mainplayer, mundo);
	playerMoveuFrenteTras = playerMoveu;
	if (mundo.teclas.moverEsquerda) playerMoveu = tentarRodarCreaturaEsquerda(mundo.mainplayer, mundo);
	if (mundo.teclas.moverDireita) playerMoveu = tentarRodarCreaturaDireita(mundo.mainplayer, mundo);
	


	//DEBUG MOVIMENTO (hugo)
	if (playerMoveu && mundo.opcoesDesenho.debugShowPlayerCordinatesOnMove)
		printf("player(X,Y,Z) %f, %f, %f Orientacao: %f Vel: %f\n", mundo.mainplayer.x, mundo.mainplayer.y, mundo.mainplayer.z, mundo.mainplayer.orientacao, mundo.mainplayer.velocidade);
		
	//incremento de indicadorMovimento para rota��es
	if (playerMoveuFrenteTras) { 
		mundo.mainplayer.indicadorMovimento += (1.0f / 10.0f);
		if (mundo.mainplayer.indicadorMovimento > 1.0f) {
			mundo.mainplayer.indicadorMovimento = 0.0f;
		}
	};

	//ALTERAR as VISTAS (Hugo)
	if (mundo.teclas.tecla01) {
		mundo.teclas.tecla01 = GL_FALSE;
		mundo.vistas.vistaActiva++;
		if (mundo.vistas.vistaActiva > VIEW_MAX_VISTAS) mundo.vistas.vistaActiva = 0;
		if (true) printf("Vista mudou para: %i\n", mundo.vistas.vistaActiva);
			

		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	}

	//Actializar os dados das vistas / cameras.
	updateView(mundo);

	int vista = mundo.vistas.vistaActiva;

	
	GLboolean canZoom = GL_TRUE;
	if (vista == 2) canZoom = GL_FALSE;   //BLOQUEAMOS O ZOOM PARA O FIRST - PERSON

	if (mundo.teclas.tecla02) {		
		if(canZoom) mundo.vistas.cameras[vista].fov--;
		if (mundo.vistas.cameras[vista].fov < 0.0) mundo.vistas.cameras[vista].fov = 0.0f;		// Impedir valores negativos.
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	}
	if (mundo.teclas.tecla03) {		//ZOOM OUT.
		if (canZoom) mundo.vistas.cameras[vista].fov++;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	}

	// Tecla de iluminacao
	if (mundo.teclas.tecla04) {
		mundo.teclas.tecla04 = GL_FALSE;
		mundo.iluminacao.iluminacaoLigada = !mundo.iluminacao.iluminacaoLigada;
		mundo.iluminacao.iluminacaoIniciada = GL_FALSE;
	}

	int jogadorNaSaida = is_jogador_na_saida(mundo);
	if (jogadorNaSaida) { 
		printf("Player is at exit.\n"); 		
		avancarLevel(mundo);
	}

	// STATUSBAR escreve no topo do ecra
	changestatusbar(mundo);  // logica.cpp
	
	logicaChaves(mundo);	// Trata da logica das chaves.

	// redesenhar o ecra 
	glutPostRedisplay();
}



void imprime_ajuda(void)
{
	printf("\n\nAJUDA:\n\n");
	printf("h,H - Ajuda \n");
	printf("w,W - Andar Frente\n");
	printf("s,S - Andar Tras\n");
	printf("a,A - Rodar Esquerda\n");
	printf("d,D - Rodar Direira\n");
	printf("1   - Mudar Vista\n");
	printf("2,3 - Zoom in/out\n");
	printf("4   - Liga/Desliga Iliminacao\n\n");
	
	//printf("i,I - Reinicia modelo\n");
	printf("o,O - Alterna entre projecao Ortografica e Perspectiva\n");
	printf("f,F - Poligono Fill \n");
	printf("l,L - Poligono Line \n");
	printf("p,P - Poligono Point \n");
	//printf("s,S - Inicia/para movimento\n");
	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interaccao via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{
	switch (key) {

	case 27:
		exit(1);
		// ... accoes sobre outras teclas ... 

	case 'w':
	case 'W':
		mundo.teclas.moverFrente = GL_TRUE;
		break;
	case 'A':
	case 'a':
		mundo.teclas.moverEsquerda = GL_TRUE;
		estado.teclas.a = GL_TRUE;
		break;
	case 'S':
	case 's':
		mundo.teclas.moverTras = GL_TRUE;
		break;
	case 'D':
	case 'd':
		mundo.teclas.moverDireita = GL_TRUE;
		break;


	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case 'i':
	case 'I':
		
		break;
	case 'o':
	case 'O':
		estado.ortho = !estado.ortho;
		Reshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
		break;
	case 'Q':
	case 'q': estado.teclas.q = GL_TRUE;
		break;

	case 'Z':
	case 'z': estado.teclas.z = GL_TRUE;
		break;
	case 'X':
	case 'x': estado.teclas.x = GL_TRUE;
		break;

		//Teclas de ZOOM IN E OUT
	case '2': mundo.teclas.tecla02 = GL_TRUE;
		break;
	case '3': mundo.teclas.tecla03 = GL_TRUE;
		break;


		
		/*
		case 'D':
		case 'd': estado.debug = !estado.debug;
		if (estado.menuActivo || mundo.pause )
		glutPostRedisplay();
		printf("DEBUG is %s\n", (estado.debug) ? "ON" : "OFF");
		break;
		*/
	case 'f':
	case 'F':
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	case 'p':
	case 'P':
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
	case 'l':
	case 'L':
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;

		/* case 's' :
		case 'S' :
		modelo.parado=!modelo.parado;
		break;*/


	}

	//if (estado.debug)		printf("Carregou na tecla %c\n", key);

}

// Callback para interaccao via teclado (largar a tecla)

void KeyUp(unsigned char key, int x, int y)
{
	switch (key) {
		// ... accoes sobre largar teclas ... 
	case 27:
		exit(1);
		// ... accoes sobre outras teclas ... 

		// TECLAS DE MOVIMENTO
	case 'w':
	case 'W':
		mundo.teclas.moverFrente = GL_FALSE;
		break;
	case 'A':
	case 'a':
		mundo.teclas.moverEsquerda = GL_FALSE;		
		break;
	case 'S':
	case 's':
		mundo.teclas.moverTras = GL_FALSE;
		break;
	case 'D':
	case 'd':
		mundo.teclas.moverDireita = GL_FALSE;
		break;

		// TECLAS DE VISTAS:
	case '1':
		mundo.teclas.tecla01 = GL_TRUE;
		break;
	case '2':
		mundo.teclas.tecla02 = GL_FALSE;
		break;
	case '3':
		mundo.teclas.tecla03 = GL_FALSE;
		break;
	case '4':
		mundo.teclas.tecla04 = GL_TRUE;
		break;


	case 'Q':
	case 'q': estado.teclas.q = GL_FALSE;
		break;
		
	case 'Z':
	case 'z': estado.teclas.z = GL_FALSE;
		break;
	case 'X':
	case 'x': estado.teclas.x = GL_FALSE;
		break;
		/*
		case 'D':
		case 'd': estado.debug = !estado.debug;
		if (estado.menuActivo || mundo.pause )
		glutPostRedisplay();
		printf("DEBUG is %s\n", (estado.debug) ? "ON" : "OFF");
		break;
		case 'f':
		case 'F':
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
		case 'p':
		case 'P':
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
		case 'l':
		case 'L':
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;

		/* case 's' :
		case 'S' :
		modelo.parado=!modelo.parado;
		break;*/


	}

	//if (estado.debug)		printf("Largou a tecla %c\n", key);
}

// Callback para interaccao via teclas especiais  (carregar na tecla)

void SpecialKey(int key, int x, int y)
{
	// ... accoes sobre outras teclas especiais ... 
	//    GLUT_KEY_F1 ... GLUT_KEY_F12
	//    GLUT_KEY_UP
	//    GLUT_KEY_DOWN
	//    GLUT_KEY_LEFT
	//    GLUT_KEY_RIGHT
	//    GLUT_KEY_PAGE_UP
	//    GLUT_KEY_PAGE_DOWN
	//    GLUT_KEY_HOME
	//    GLUT_KEY_END
	//    GLUT_KEY_INSERT 

	switch (key) {

		// redesenhar o ecra 
		//glutPostRedisplay();
	}


	if (estado.debug)
		printf("Carregou na tecla especial %d\n", key);
}

// Callback para interaccao via teclas especiais (largar na tecla)

void SpecialKeyUp(int key, int x, int y)
{
	switch (key) {
	}
	if (estado.debug)
		printf("Largou a tecla especial %d\n", key);

}

int run(int argc, char **argv)
{
	char str[] = " makefile MAKEFILE Makefile ";
	estado.doubleBuffer = 1;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(640, 480);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Exemplo") == GL_FALSE)
		exit(1);

	Init();

	imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Draw);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// callbacks timer/idle
	glutTimerFunc(estado.delayMovimento, Timer, 0);

	// COMECAR...
	glutMainLoop();
	return 0;
}
