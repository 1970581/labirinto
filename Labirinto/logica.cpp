#include "stdafx.h"
#include "logica.h"
#include <GL/glut.h>
#include <math.h>
#include "iniciador.h"


void test(int x) {
	int y = x + 1;
	//return 0;
	
}

// Copia uma creatura.
void copiarCreatura(Creatura original, Creatura &clone) {
	clone.altura = original.altura;
	clone.comprimento = original.comprimento;
	clone.largura = original.largura;
	clone.orientacao = original.orientacao;
	clone.velocidade = original.velocidade;
	clone.velocidadeRotacao = original.velocidadeRotacao;
	clone.x = original.x;
	clone.y = original.y;
	clone.z = original.z;
}

// Tenta mover o jogador para a frente. Verifica colisoes. Se colidir retorna falso, e nao mexe o jogador.
int tentarMoverCreaturaFrente(Creatura &creatura,Mundo &mundo) {

	// TODO METODO DE TESTE SUBSTITUIR mexe 0.10 metros para a frente.
	Pos novaPos;
		
	// TODO  SUBSTITUIR
	// METODO BASICO mexe 0.10 metros para a frente.
	frontFromPlayer(novaPos, creatura);
		
	Creatura clone;		// Creatura em nova posicao.
	copiarCreatura(creatura, clone);

	clone.x = novaPos.x;
	clone.y = novaPos.y;

	int colisao = isCreaturaEmColisaoBasica(clone, mundo);
	
	if (colisao) {
		if ( mundo.opcoesDesenho.debugShowConsoleColision) printf("Colisao basica detectada!!!!\n");
		return 0;
		
	}
	creatura.x = novaPos.x;
	creatura.y = novaPos.y;

	return 1; //true 1, false 0
}

// Tenta mover o jogador para a tras. Verifica colisoes. Se colidir retorna falso, e nao mexe o jogador.
int tentarMoverCreaturaTras(Creatura &creatura, Mundo &mundo) {
		
	// TODO  SUBSTITUIR
	// METODO BASICO mexe 0.10 metros para tras.
	Pos novaPos;
	backFromPlayer(novaPos, creatura);

	Creatura clone;		// Creatura em nova posicao.
	copiarCreatura(creatura, clone);

	clone.x = novaPos.x;
	clone.y = novaPos.y;

	int colisao = isCreaturaEmColisaoBasica(clone, mundo);

	if (colisao) {
		if ( mundo.opcoesDesenho.debugShowConsoleColision) printf("Colisao basica detectada!!!!\n");
		return 0;

	}
	creatura.x = novaPos.x;
	creatura.y = novaPos.y;

	return 1; //true 1, false 0
}


// Tenta rodar o jogador para a esquerda. Verifica colisoes. Se colidir retorna falso, e nao mexe o jogador.
int tentarRodarCreaturaEsquerda(Creatura &creatura,Mundo &mundo) {

	Creatura clone;		// Creatura em nova posicao.
	copiarCreatura(creatura, clone);

	clone.orientacao += clone.velocidadeRotacao;
	if (clone.orientacao > 360.0f) clone.orientacao = 0.0f;

	int colisao = isCreaturaEmColisaoBasica(clone, mundo);

	if (colisao) {
		if ( mundo.opcoesDesenho.debugShowConsoleColision) printf("Colisao basica detectada!!!!\n");
		return 0;
	}
	
	creatura.orientacao = clone.orientacao;

	return 1; //true 1, false 0
}

// Tenta rodar o jogador para a direita. Verifica colisoes. Se colidir retorna falso, e nao mexe o jogador.
int tentarRodarCreaturaDireita(Creatura &creatura, Mundo &mundo) {

	
	Creatura clone;		// Creatura em nova posicao.
	copiarCreatura(creatura, clone);


	clone.orientacao -= clone.velocidadeRotacao;	
	if (clone.orientacao < 0.0f) clone.orientacao = 359.0f;

	int colisao = isCreaturaEmColisaoBasica(clone, mundo);

	if (colisao) {
		if (mundo.opcoesDesenho.debugShowConsoleColision) printf("Colisao basica detectada!!!!\n");
		return 0;
	}
	
	creatura.orientacao = clone.orientacao;

	return 1; //true 1, false 0
}


// Calculo de colisoes de creaturas.
//                     Frente
//                       /\
//                       ||               A distancia frente a traz � Creatura.comprimento
//                       \/
//       Esquerda <->  Centro  <->     Direita
//                       /\
//                       ||               A distancia de Esquerda a Direita � Creatura.largura.
//                       \/
//                      Traz
//
//  Verificamos apenas se algum destes 5 pontos Frente,Esquerda,Direita,Traz,Centro
//  esta dentro de uma parede e se estiver nao movemos.
//  Atencao ao rodar, estamos a mexer todos os 4 pontos exteriores.

// Calcular os pontos com base em angulo e distancia, para as colicoes
// https://stackoverflow.com/questions/12959237/get-point-coordinates-based-on-direction-and-distance-vector 

void frontFromPlayer(Pos &pos, Creatura player) {
	GLfloat angle = (player.orientacao * M_PI )/ 180.0f;  // Passar de graus a radianos.
	GLfloat distancia = player.comprimento / 2.0f;   

	pos.x = player.x + cos(angle) * distancia;    // unchanged
	pos.y = player.y + sin(angle) * distancia;    // minus on the Sin
	pos.z = player.z;
}

void leftFromPlayer(Pos &pos, Creatura player) {
	GLfloat angle = (player.orientacao * M_PI) / 180.0f;
	angle += 0.50f * M_PI ;						// Adicionar + 90� para termos a esquerda.
	GLfloat distancia = player.largura / 2.0f;
		
	pos.x = player.x + cos(angle) * distancia;    // unchanged
	pos.y = player.y + sin(angle) * distancia;    // minus on the Sin
	pos.z = player.z;
}

void backFromPlayer(Pos &pos, Creatura player) {
	GLfloat angle = (player.orientacao * M_PI) / 180.0f;
	angle += 1.00f * M_PI;						// Adicionar + 180� para termos a traz.
	GLfloat distancia = player.comprimento / 2.0f;
		
	pos.x = player.x + cos(angle) * distancia;    // unchanged
	pos.y = player.y + sin(angle) * distancia;    // minus on the Sin
	pos.z = player.z;
}

void rightFromPlayer(Pos &pos, Creatura player) {
	GLfloat angle = (player.orientacao * M_PI) / 180.0f;
	angle += 1.50f * M_PI;						// Adicionar + 270� para termos a direita.
	GLfloat distancia = player.largura / 2.0f;

	pos.x = player.x + cos(angle) * distancia;    // unchanged
	pos.y = player.y + sin(angle) * distancia;    // minus on the Sin
	pos.z = player.z;
}

// Verifica a logica se o tipo de parede � solido ou nao
int isTypoParedeSolida(GLint typoParede, Mundo mundo) {
	int solido = 1;
	int vazio = 0;

	switch (typoParede) {

	case WALL_EMPTY:
		return vazio;
		break;
	case WALL_OBJECT_EXIT:
		return vazio;
	case WALL_OBJECT_KEY_1:
		return vazio;
		break;
	case WALL_OBJECT_DOOR_1:
		if (mundo.door.apanheiKey1) return vazio;
		else return solido;
		break;
	case WALL_SOLID:
		return solido;
		break;	
	default:
		return vazio;
	}

}

// Verifica se a parede que se encontra na celula X,Y � solida ou nao.
int isCellSolid(int x, int y, Mundo mundo) {

	int foraMapa = 0; // Falso, se estivermos FORA do mapa. 

	int maxCellX = mundo.map_cell_max_x;
	int maxCellY = mundo.map_cell_max_y;
	if (x < 0 || x >= maxCellX || y < 0 || y >= maxCellY) return foraMapa;

	GLint tipoParede = mundo.mapa.cell[x][y];

	return isTypoParedeSolida(tipoParede, mundo);  //0 false, 1 true
}

// Dado uma posicao, devolve qual a o x e y da celula.
void qualACelulaDestePonto(Pos posicao, int &xCell, int &yCell, Mundo mundo) {

	//   0 1 2 3 4 5 6 7 8 9 10 
	//             # - celula do meio (0,0)
	//   |--------| 
	//   numero de celula de metade = ate a celula central

	int ate_a_celula_central_x = mundo.map_cell_max_x / 2;
	int ate_a_celula_central_y = mundo.map_cell_max_y / 2;

	int celula_central_x = (mundo.map_cell_max_x / 2 )+ 1;
	int celula_central_y = (mundo.map_cell_max_y / 2) + 1;
		
	GLfloat lengthX = mundo.defaultCell.cell_largura_x;
	GLfloat lengthY = mundo.defaultCell.cell_largura_y;

	GLfloat comprimentoMetadeLabirinto_X = (lengthX * ate_a_celula_central_x) + (lengthX / 2);
	GLfloat x = posicao.x + comprimentoMetadeLabirinto_X;
	x = x / (lengthX);
		
	GLfloat comprimentoMetadeLabirinto_Y = (lengthY * ate_a_celula_central_y) + (lengthY / 2);
	GLfloat y = posicao.y + comprimentoMetadeLabirinto_Y;
	y = y / lengthY;

	xCell = (int)x;		//Conversao de float para int.
	yCell = (int)y;
	
	

	// Protecao para detectar erros de convers�o GLfloat to int
	GLfloat testGLfloatConversionToInt = (float)25;
	int expected = 2;
	testGLfloatConversionToInt /= 10;
	int actual = (int)testGLfloatConversionToInt;
	if (actual != expected) printf("WARNING ********************\n GLfloat to int conversion failure at qualACelulaDestePonto in logica.ccp\n");
}

//Obtem o ponto central de uma celula
void getCentroDeCelula(int cellX, int cellY, Pos &posicaoCentralDaCelula, Mundo mundo) {
	int ate_a_celula_central_x = mundo.map_cell_max_x / 2;
	int ate_a_celula_central_y = mundo.map_cell_max_y / 2;
	

	GLfloat lengthX = mundo.defaultCell.cell_largura_x;
	GLfloat lengthY = mundo.defaultCell.cell_largura_y;


	GLfloat comprimentoMetadeLabirinto_X = (lengthX * ate_a_celula_central_x) + (lengthX / 2);
	
	//            0  - comprimento de metade do lab + meia celula + numero celulas x comprimento de celula
	GLfloat x = 0.0f - comprimentoMetadeLabirinto_X + (lengthX/2) + (cellX * lengthX);
	

	GLfloat comprimentoMetadeLabirinto_Y = (lengthY * ate_a_celula_central_y) + (lengthY / 2);
	GLfloat y = 0.0f - comprimentoMetadeLabirinto_Y + (lengthX / 2) + (cellY * lengthY);

	posicaoCentralDaCelula.x = x;
	posicaoCentralDaCelula.y = y;
	posicaoCentralDaCelula.z =mundo.zero_z;
}



//Calculo se um ponto esta em colisao Basico so XY
int isPosicaoEmColisaoBasica(Pos pos, Mundo mundo) {
	int colisao = 1;
	int naoColisao = 0;
	int xCell = 0;
	int yCell = 0;

	qualACelulaDestePonto(pos, xCell, yCell, mundo);
	int solid = isCellSolid(xCell, yCell, mundo);
	if (solid) return colisao;  //0 false, 1 true
	return naoColisao;
}

//Calculo de colisoes de creaturas, verifica os 5 pontos se estao dentro de paredes : Frente,Esquerda,Direita,Traz,Centro
int isCreaturaEmColisaoBasica(Creatura creatura, Mundo mundo) {
	int colisao = 1;
	int naoColisao = 0;

	//Centro
	Pos pos;
	pos.x = creatura.x; pos.y = creatura.y; pos.z = creatura.z;
	if (isPosicaoEmColisaoBasica(pos, mundo)) return colisao;
	
	// Frente
	frontFromPlayer(pos, creatura);
	if (isPosicaoEmColisaoBasica(pos, mundo)) return colisao;

	// Tras
	backFromPlayer(pos, creatura);
	if (isPosicaoEmColisaoBasica(pos, mundo)) return colisao;

	//Esquerda
	leftFromPlayer(pos, creatura);
	if (isPosicaoEmColisaoBasica(pos, mundo)) return colisao;

	//Direita
	rightFromPlayer(pos, creatura);
	if (isPosicaoEmColisaoBasica(pos, mundo)) return colisao;

	return naoColisao;
}


// Retorna o GL_Material para os defaults, usado por causa de objectos que ainda nao implementam o GL_Material.
void resetMaterialColorHugo() {
	//https://www.gamedev.net/forums/topic/283067-reset-material-values-to-default/

	// RGB
	GLfloat corAmbiente[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat corDiffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat corSpecular[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat corEmission[] = { 0.0f, 0.0f, 0.0f, 1.0f };

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, corAmbiente);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, corDiffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, corSpecular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, corEmission);


}

// Verifica se o jogador esta na saida. Fazemos batota, verificando apenas se esta numa celula de saida.
int is_jogador_na_saida(Mundo &mundo) {
	int naSaida = 1;
	int falso = 0;
	Pos jogador;
	jogador.x = mundo.mainplayer.x;
	jogador.y = mundo.mainplayer.y;
	jogador.z = mundo.mainplayer.z;
	int xCell = 0;
	int yCell = 0;

	qualACelulaDestePonto(jogador, xCell, yCell, mundo);
	
	GLint tipoParedeObjecto = mundo.mapa.cell[xCell][yCell];

	if (tipoParedeObjecto == WALL_OBJECT_EXIT) return naSaida;
	else return falso;
}

// Avance o nivel, e termina quando chegar ao fim.
void avancarLevel(Mundo &mundo) {
	mundo.levels.actualLevel++;
	if (mundo.levels.actualLevel > mundo.levels.maxLevel) exit(0);
	//iniciarMundo(mundo);		//desnecesarior fazer reset total ao mundo.
	mundo.mainplayer.x = DEFAULT_PLAYER_X;
	mundo.mainplayer.y = DEFAULT_PLAYER_Y;
	mundo.mainplayer.z = DEFAULT_PLAYER_Z;

	// mudar o mapa do nivel para o proximo...
	//Nivel zero nao existe...
	int nivel = mundo.levels.actualLevel -1;
	if (nivel < 0) nivel = 0;
	int x = 0;
	int y = 0;

	for (y = 0; y < MAP_MAX_Y_CELLS; y++) {
		for (x = 0; x < MAP_MAX_X_CELLS; x++) {
			mundo.mapa.cell[x][y] = mundo.levels.niveis[nivel][x][y];
		}
	}

	// UPDATE DAS CHAVES.
	mundo.door.apanheiKey1 = GL_FALSE;
	mundo.door.grauDeApanhoDaChaves = 1.0f;
	mundo.door.abertaPorta1 = GL_FALSE;
	mundo.door.grauDeFechoDaPorta1 = 1.0f;
	mundo.door.rotacaoChaves = 0.00f;
}


// Faz o update do texto mostar na barra do topo.
void changestatusbar(Mundo &mundo) {	

	char key = ' ';
	if (mundo.door.apanheiKey1) key = '1';

	sprintf_s(mundo.texto.statusbar, "Nivel: %i/%i Key: %c", mundo.levels.actualLevel, mundo.levels.maxLevel, key);

	if (mundo.levels.actualLevel == 1) 	sprintf_s(mundo.texto.statusbar, "Nivel: %i/%i Key: %c Boa Sorte! ", mundo.levels.actualLevel, mundo.levels.maxLevel, key);
	
	if (mundo.levels.actualLevel == mundo.levels.maxLevel) 	sprintf_s(mundo.texto.statusbar, "Nivel: %i/%i Key: %c Ultimo! ", mundo.levels.actualLevel, mundo.levels.maxLevel,key);

}


// Trata da logica das chaves e portas.
void logicaChaves(Mundo &mundo) {

	GLfloat nivelRotacaoChaves = 0.005f;
	mundo.door.rotacaoChaves += nivelRotacaoChaves;
	if (mundo.door.rotacaoChaves > 1.00f) mundo.door.rotacaoChaves = 0.0f;

	int xCell, yCell;
	Pos apanhar;
	apanhar.x = mundo.mainplayer.x;
	apanhar.y = mundo.mainplayer.y;
	apanhar.z = mundo.mainplayer.z;

	qualACelulaDestePonto(apanhar, xCell, yCell, mundo);
	int tipoObjecto = mundo.mapa.cell[xCell][yCell];

	if (tipoObjecto == WALL_OBJECT_KEY_1) mundo.door.apanheiKey1 = GL_TRUE;
	GLfloat nivelDeReducao = 0.005f;
	if (mundo.door.apanheiKey1 && mundo.door.grauDeApanhoDaChaves > 0) mundo.door.grauDeApanhoDaChaves -= nivelDeReducao;
}