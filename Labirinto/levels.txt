
# Nivel 1
# Tem de ter 21 por 21 senao da erro. 
# A linhas do nivel comecam com dois pontos
# A indicacao para avancar para o proximo nivel e um dollar seguido de NEXT
# e saida
# A porta 1
# a key 1

:XXXXXXXXXXXXXXXXXXXXX
:X                   X
:X         X         X
:X  X X    XX        X
:X   X  XXXXXX       X

:X  X X    XX        X
:X X       X         X
:X XXXXXXXXXXXXXX    X
:X              X    X
:X              X    X

:X            a X e  X

:X              XXXXXX
:XXXXXX XXXXXXXXX    X
:X    XAX       X    X
:X    X X       X    X
:X    X X       X    X

:X     e             X
:X                   X
:X                   X
:X                   X
:XXXXXXXXXXXXXXXXXXXXX

$NEXT

:XXXXXXXXXXXXXXXXXXXXX
:X e              X  X
:XXXXXXXXXXXXXXXX    X
:X              X XXXX
:X  XXXXXXX XXX X    X
:X  X     X   X XXX  X
:X XXa X  X   XXX    X
:X    XX  X          X
:XXXXXXX  XXXXXXXXXXXX
:X             X     X
:X             A   e X
:X             X     X
:X             XXXXXXX
:X                   X
:X                   X
:X                   X
:X                   X
:X                   X
:X                   X
:X                   X
:XXXXXXXXXXXXXXXXXXXXX

$NEXT

:XXXXXXXXXXXXXXXXXXXXX
:X                   X
:X                   X
:X                   X
:X                   X
:X    XXXXXXXXXXX    X
:X    X         X    X
:X    X         X    X
:X    X  XX XX  X    X
:X    X  X   X  X    X
:X   a   X   X  A  e X
:X    X  X   X  X    X
:X    X  XX XX  X    X
:X    X         X    X
:X    X         X    X
:X    XXXXXXXXXXX    X
:X                   X
:X                   X
:X                   X
:X                   X
:XXXXXXXXXXXXXXXXXXXXX

$NEXT
