
#include "stdafx.h"
#include "global.h"
#include "iniciador.h"
#include <GL/glut.h>



// Local onde iniciamos as extroturas iniciais do jogo.

void iniciarMundo(Mundo &meuMundo) {
	
	iniciarOpcoesdesenho(meuMundo.opcoesDesenho);
	meuMundo.opcoesDesenho.debugMapOnConsole = true;

	meuMundo.metro = DEFAULT_UNIT ;
	meuMundo.map_cell_max_x = MAP_MAX_X_CELLS;
	meuMundo.map_cell_max_y = MAP_MAX_Y_CELLS;

	// Verificação que temos numero impar de celulas x e y
	if (meuMundo.map_cell_max_x % 2 == 0) { 
		meuMundo.map_cell_max_x++;
		printf("O MAPA TEM NUMERO IMPAR DE CELULAS X Adicionada +1 \n");
	}
	if (meuMundo.map_cell_max_y % 2 == 0) { 
		printf("O MAPA TEM NUMERO IMPAR DE CELULAS Y Adicionada +1 \n");
		meuMundo.map_cell_max_x++; 
	}
	
	meuMundo.zero_x = MAP_DEFAULT_ZERO_X * meuMundo.metro;
	meuMundo.zero_y = MAP_DEFAULT_ZERO_Y * meuMundo.metro;
	meuMundo.zero_z = MAP_DEFAULT_ZERO_Z * meuMundo.metro;

	iniciarJogador(meuMundo.mainplayer, meuMundo);		//init do jogador
	iniciarCellDefenitions(meuMundo.defaultCell, meuMundo); //init da wall de referencia
	iniciarMapa(meuMundo.mapa, meuMundo);				//init do mapa das paredes, array de celulas.
	
	// Se debug, output do mapa para a consola.
	if (meuMundo.opcoesDesenho.debugMapOnConsole) {
		printf("\n MAPA: \n");
		for (int x = 0;x < meuMundo.map_cell_max_x;x++) {
			for (int y = 0;y < meuMundo.map_cell_max_y;y++) {
				printf("%2i ", meuMundo.mapa.cell[x][y]);
			}
			printf("\n");
		}
	}

	iniciarCameras(meuMundo);			//Inicia as posicoes das cameras;
	iniciarIluminacao(meuMundo);		//Inicia as variaveis de iluminacao
	iniciarQuadraticas(meuMundo);		//Inicializa as quadraticas.
	iniciarLevels(meuMundo);			//Inicializa os niveis.

}

// Indica as opcoes do que devemoos ligar para desenhar.
void iniciarOpcoesdesenho(OpcoesDesenho &opcoesDesenho) {
	opcoesDesenho.debugMapOnConsole = DEBUG_ON;
	opcoesDesenho.debugMapOnConsole = DEBUG_OFF;
	opcoesDesenho.debugFloor = DEBUG_OFF;
	opcoesDesenho.debugShowPlayerColisionArea = DEBUG_ON;
	opcoesDesenho.debugShowPlayerColisionArea = DEBUG_OFF;
	
	opcoesDesenho.debugWalls = DEBUG_ON;
	opcoesDesenho.debugWalls = DEBUG_OFF;
	
	opcoesDesenho.debugShowPlayerCordinatesOnMove = GL_TRUE;
	opcoesDesenho.debugShowPlayerCordinatesOnMove = GL_FALSE;
	
	opcoesDesenho.debugShowConsoleColision = GL_TRUE;
	opcoesDesenho.debugShowConsoleColision = GL_FALSE;

	opcoesDesenho.realWalls = GL_TRUE;
	opcoesDesenho.realFloor = GL_TRUE;
	opcoesDesenho.realPlayer = GL_TRUE;
}

void iniciarJogador(Creatura &jogador, Mundo meuMundo) {
	jogador.x = DEFAULT_PLAYER_X * meuMundo.metro;
	jogador.y = DEFAULT_PLAYER_Y * meuMundo.metro;
	jogador.z = DEFAULT_PLAYER_Z * meuMundo.metro;
	jogador.altura = DEFAULT_PLAYER_ALTURA * meuMundo.metro;
	jogador.largura = DEFAULT_PLAYER_LARGURA * meuMundo.metro;
	jogador.comprimento = DEFAULT_PLAYER_COMPRIMENTO * meuMundo.metro;
	jogador.orientacao = DEFAULT_PLAYER_ORIENTACAO * meuMundo.metro;
	jogador.velocidade = DEFAULT_PLAYER_VELOCIDADE * meuMundo.metro;
	jogador.velocidadeRotacao = DEFAULT_TURN_RATE_DEGREES;
	jogador.indicadorMovimento = 0.0f;
}

void iniciarCellDefenitions(Cell &cellDefenition, Mundo mundo) {
	cellDefenition.cell_largura_x = DEFAULT_CELL_TAMANHO_X * mundo.metro;
	cellDefenition.cell_largura_y = DEFAULT_CELL_TAMANHO_Y * mundo.metro;
	cellDefenition.cell_altura_z = DEFAULT_CELL_TAMANHO_Z * mundo.metro;
}

void iniciarMapa(Mapa &mapa, Mundo mundo) {
	//limpar lixo/garbage da memoria
	int x = 0;
	int y = 0;
	GLint X = WALL_SOLID;
	GLint o = WALL_EMPTY;
	GLint E = WALL_OBJECT_EXIT;

	//   ________________
	//   |    y ->      |
	//   |  x           |
	//   |  ||          |
	//   |  \/          |
	//   |              |
	//   ----------------

	// mapa a inicializar.           // O MAPA ESTA MORTO. USEM O levels.txt no folder RESORCES.
	GLint zeroMap[] = {
		 X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,

		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X, o, X, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X, o, X, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X, o, X, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X, o, X, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X, o, X, o, X,

		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, X, o, X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, X, o, X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,

		 X, o, o, o, X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, X, o, X, o, X, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, X, X, X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, X, o, o, o, o, E, o, o, o, o, o, o, o, o, o, o, X,
		 X, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, X,

		 X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X, X


	};
	int maxZeroMap = sizeof(zeroMap) / sizeof(GLint);		printf("%i\n", maxZeroMap);
	int i = 0;

	//Copiar o mapa para o nossa estrotura.
	for (x = 0; x < mundo.map_cell_max_x;x++) {
		for (y = 0; y < mundo.map_cell_max_y;y++) {
			mapa.cell[x][y] = WALL_EMPTY;
			if (i < maxZeroMap) {  // Evitar segmentation fault
				mapa.cell[x][y] = zeroMap[i]; 
				
				//printf("%2i", mapa.cell[x][y]);
			}
			
			i++;		
		}
		//printf("\n");
	}

}

// Inicia os dados das vistas das cameras.
void iniciarCameras(Mundo &mundo) {

	int view = 0;

	view = 0; // Vista Inicial DEFAULT   VISTA DE TOPO

	// Olho / posicao da camera.
	mundo.vistas.cameras[view].eye.x = 40.0;
	mundo.vistas.cameras[view].eye.y = 40.0;
	mundo.vistas.cameras[view].eye.z = 40.0;
	// Centro, local para onde apontamos a camara
	mundo.vistas.cameras[view].center.x = 0.0;
	mundo.vistas.cameras[view].center.y = 0.0;
	mundo.vistas.cameras[view].center.z = 0.0;
	// Vector que indica o up da camera, para ficar horisontal.
	mundo.vistas.cameras[view].up.x = 0.0;
	mundo.vistas.cameras[view].up.y = 0.0;
	mundo.vistas.cameras[view].up.z = 1.0;
	// largura de abertura, faz zoom.
	mundo.vistas.cameras[view].fov = 60.0;
	
	// IMPORTANTE!!!!
	// O ABAIXO BASICAMENTE SO USADO PARA COLOCAR O FIELD OF VIEW 
	// E NAO TERMOS VALORES NULL
	// O RESTO SERA SEMPRE RESCRITO AO FAZER UPDATE DA VIEW. (change view)

	view = 1; //    VISTA QUE SEGUE POR CIMA

			  // Olho / posicao da camera.
	mundo.vistas.cameras[view].eye.x = 40.0;
	mundo.vistas.cameras[view].eye.y = 40.0;
	mundo.vistas.cameras[view].eye.z = 40.0;
	// Centro, local para onde apontamos a camara
	mundo.vistas.cameras[view].center.x = 0.0;
	mundo.vistas.cameras[view].center.y = 0.0;
	mundo.vistas.cameras[view].center.z = 0.0;
	// Vector que indica o up da camera, para ficar horisontal.
	mundo.vistas.cameras[view].up.x = 0.0;
	mundo.vistas.cameras[view].up.y = 0.0;
	mundo.vistas.cameras[view].up.z = 1.0;
	// largura de abertura, faz zoom.
	mundo.vistas.cameras[view].fov = 60.0;

	view =  2; //    VISTA FIRST-PERSON

			  // Olho / posicao da camera.
	mundo.vistas.cameras[view].eye.x = 40.0;
	mundo.vistas.cameras[view].eye.y = 40.0;
	mundo.vistas.cameras[view].eye.z = 40.0;
	// Centro, local para onde apontamos a camara
	mundo.vistas.cameras[view].center.x = 0.0;
	mundo.vistas.cameras[view].center.y = 0.0;
	mundo.vistas.cameras[view].center.z = 0.0;
	// Vector que indica o up da camera, para ficar horisontal.
	mundo.vistas.cameras[view].up.x = 0.0;
	mundo.vistas.cameras[view].up.y = 0.0;
	mundo.vistas.cameras[view].up.z = 1.0;
	// largura de abertura, faz zoom.
	mundo.vistas.cameras[view].fov = 60.0;



}

// Inicia os dados da iluminacao.
void iniciarIluminacao(Mundo &mundo) {

	mundo.iluminacao.iluminacaoIniciada = GL_FALSE;
	mundo.iluminacao.iluminacaoLigada = GL_FALSE;

}

// Inicializa as quadraticas.
void iniciarQuadraticas(Mundo &mundo) {

	//Esfera de saida HUGO
	mundo.quadraticas.objecto[0] = gluNewQuadric();
	gluQuadricDrawStyle(mundo.quadraticas.objecto[0], GLU_FILL);

	//Chaves 1 Hugo
	mundo.quadraticas.objecto[1] = gluNewQuadric();
	gluQuadricDrawStyle(mundo.quadraticas.objecto[1], GLU_FILL);
	mundo.quadraticas.objecto[2] = gluNewQuadric();
	gluQuadricDrawStyle(mundo.quadraticas.objecto[2], GLU_FILL);

	//quadraticas boneco
	int i = 0;
	for (i = 0; i < 10; i++) {
		mundo.quadraticas.boneco[i] = gluNewQuadric();
		gluQuadricDrawStyle(mundo.quadraticas.boneco[i], GLU_FILL);
	}
}

void iniciarLevels(Mundo &mundo) {
	mundo.levels.maxLevel = LEVEL_MAX_LEVEL;
	if(mundo.levels.actualLevel <= LEVEL_START_LEVEL ) mundo.levels.actualLevel = LEVEL_START_LEVEL;		// Inicializa a zero.

	// Vamos zerar todos os niveis para evitar erros.....
	int nivel = 0;
	int x = 0;
	int y = 0;
	for (nivel = 0; nivel < LEVEL_MAX_LEVEL; nivel++) {
		for (y = 0; y < MAP_MAX_Y_CELLS; y++) {
			for (x = 0; x < MAP_MAX_X_CELLS; x++) {
				mundo.levels.niveis[nivel][x][y] = WALL_EMPTY;
			}
		}
	}
}