#pragma once

#ifndef INICIADOR_H
#define INICIADOR_H

#include "global.h"
void iniciarMundo(Mundo &meuMundo);
void iniciarOpcoesdesenho(OpcoesDesenho & opcoesDesenho);
void iniciarJogador(Creatura &jogador, Mundo meuMundo);
void iniciarCellDefenitions(Cell &cellDefenition, Mundo mundo);
void iniciarMapa(Mapa &mapa, Mundo mundo);
void iniciarCameras(Mundo & mundo);
void iniciarIluminacao(Mundo & mundo);
void iniciarQuadraticas(Mundo & mundo);
void iniciarLevels(Mundo & mundo);
#endif