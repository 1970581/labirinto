#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS
#include "logica.h"
#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "levelLoader.h"

int load_all_levels(Mundo &mundo) {

	int max_x = MAP_MAX_X_CELLS;
	int max_y = MAP_MAX_Y_CELLS;

	const char filename[256] = "levels.txt";
	FILE *fp;
	fp = fopen(filename, "r");		//Abrir o ficheiro em modo de leitura apenas.

	if (fp == NULL) {
		printf("Erro abrir ficheiro.\n");
		return 2;
	}

	int level = 0;
	int y = 0;	

	char c = ' ';
	char linha[1024];
	while (c != EOF) {
		c = fgetc(fp);
		//printf("%c", c);
		if (c == ':') {
			fgets(linha, max_x + 1, fp);
			//fscanf(fp, "%255s", linha);
			processar_linha(mundo, level, y, linha);

			y++;
		}
		if (c == '$') {
			fgets(linha, 5, fp);
			printf("%s", linha);
			int diferenca = strcmp("NEXT", linha);  //COSTUMA DAR PROBLEMAS...
			if (diferenca == 0) {
				level++;
				y = 0;
			}
		}

	}


	int closeReturn = fclose(fp);	//Fechamos o ficheiro.
	
	return 0;
}

// Adiciona uma linha de celulas X ao mapa.
int processar_linha(Mundo &mundo, int level, int y, char linha[]) {
	int max_x = MAP_MAX_X_CELLS;
	int max_y = MAP_MAX_Y_CELLS;
	int max_level = LEVEL_MAX_LEVEL;

	if(mundo.opcoesDesenho.debugMapOnConsole) printf("\n%s", linha);

	int tamanho = strlen(linha);
	if (tamanho < max_x) return 2;		//Erro, nao tem linhas suficientes.
	if (level > max_level) return 2;
	if (level < 0) return 2;

	char cc = ' ';
	int tipoParede = WALL_EMPTY;

	int x = 0;
	for (x = 0; x < max_x; x++) {

		cc = linha[x];

		switch (cc) {		//Switch, para escolhermos o tipo de parede adequado.

		case ' ':
			tipoParede = WALL_EMPTY;
			break;
		case 'X':
		case 'x':
			tipoParede = WALL_SOLID;
			break;
		case 'E':
		case 'e':
			tipoParede = WALL_OBJECT_EXIT;
			break;

		case 'A':
			tipoParede = WALL_OBJECT_DOOR_1;
			break;
		case 'a':
			tipoParede = WALL_OBJECT_KEY_1;
			break;

		default:
			tipoParede = WALL_SOLID;
			;
		};

		int yCorrigido = max_y - 1 - y;
		//mundo.levels.niveis[level][x][y] = tipoParede;
		mundo.levels.niveis[level][x][yCorrigido] = tipoParede;
	}
	return 0;

}