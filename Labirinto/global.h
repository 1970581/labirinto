#pragma once

#include <GL/glut.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#ifndef GLOBAL_H
#define GLOBAL_H

// DEFICINOES GLOBAIS.

#define DEBUG_WORLD 1
#define DEBUG_ON 1
#define DEBUG_OFF 0


//tamanho do mapa em celulas. ATENCAO SE MUDAREM VALORES, TEM DE FAZER UM NOVO MAPA no iniciador.cpp
#define MAP_MAX_X_CELLS 21
#define MAP_MAX_Y_CELLS 21

#define MAP_DEFAULT_ZERO_X 0
#define MAP_DEFAULT_ZERO_Y 0
#define MAP_DEFAULT_ZERO_Z 0

//Unidades em metros.
#define DEFAULT_UNIT 1.0f

//definicoes das celular e portanto paredes.
#define DEFAULT_CELL_TAMANHO_X 2.0f
#define DEFAULT_CELL_TAMANHO_Y 2.0f
#define DEFAULT_CELL_TAMANHO_Z 2.0f

//definicoes do jogador
#define DEFAULT_PLAYER_X 0.0f
#define DEFAULT_PLAYER_Y 0.0f
#define DEFAULT_PLAYER_Z 0.0f
#define DEFAULT_PLAYER_ALTURA 1.8f
#define DEFAULT_PLAYER_LARGURA 0.4f
#define DEFAULT_PLAYER_COMPRIMENTO 0.2f
#define DEFAULT_PLAYER_ORIENTACAO 0.0f
#define DEFAULT_PLAYER_VELOCIDADE 0.0f
#define DEFAULT_TURN_RATE_DEGREES 3.0f


// Definicoes de tipos de walls.   ATENCAO SE ADICIONADOS, ADICIONAR A levelLoader.cpp e logica.cpp
#define WALL_EMPTY 0
#define WALL_OBJECT_EXIT 1
#define WALL_OBJECT_KEY_1 11
#define WALL_OBJECT_DOOR_1 21

#define WALL_SOLID 50

#define VIEW_MAX_VISTAS 2

//Levels
#define LEVEL_MAX_LEVEL 3
#define LEVEL_START_LEVEL 0


// As nossas definicoes de ESTROTURAS do labirinto.
typedef struct {
	GLfloat    x, y, z;
}Pos;


//Defenicoes do jogador/creatura          ########## SE ALTERAREM, alterem tambem o copiarCreatura em "logica.ccp"
typedef struct {
	GLfloat x, y, z; // posicao
	GLfloat velocidade;
	GLfloat velocidadeRotacao;
	GLfloat orientacao; //direcao para onde esta virado 
	GLfloat altura;
	GLfloat largura;
	GLfloat comprimento;
	GLfloat indicadorMovimento;
} Creatura;

//Defenicoes de uma wall. Usada como default.
typedef struct {
	GLfloat cell_largura_x;
	GLfloat cell_largura_y;
	GLfloat cell_altura_z;
} Cell;

// Definicoes do mapa
typedef struct {
	GLint cell[MAP_MAX_X_CELLS][MAP_MAX_Y_CELLS];
} Mapa;

typedef struct {
	GLfloat debugMapOnConsole;						// mostra o mapa ao criar em modo de texto na consola
	GLint	debugWalls;								// mostra as paredes de debug para testes de colisao.
	GLint	debugFloor;								// mostra um chao para debug de celulas.
	GLint	debugShowPlayerColisionArea;			// mostra uma referencia para teste de colisao de jogador
	GLfloat debugShowPlayerCordinatesOnMove;		// mostra as cordinadas do jogador na consola quando move.
	GLfloat debugShowConsoleColision;				// mostra na consola que ocorreu uma colisao.
	GLboolean realWalls;
	GLboolean realFloor;
	GLboolean realPlayer;
} OpcoesDesenho;

// Extrotura com as nossas teclas.
typedef struct {
	GLboolean moverFrente;
	GLboolean moverDireita;
	GLboolean moverEsquerda;
	GLboolean moverTras;
	GLboolean tecla01; // Alterna vistas.
	GLboolean tecla02; // Zoom in out
	GLboolean tecla03;
	GLboolean tecla04; // Liga desliga Luz
} TeclasLabirinto;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;	
} Camera;


// Extrotura para as views
typedef struct {
	GLint vistaActiva;
	Camera cameras[VIEW_MAX_VISTAS+1];
} Vistas;

//Extrotura de Iluminacao
typedef struct {
	GLboolean iluminacaoLigada;
	GLboolean iluminacaoIniciada;
} Iluminacao;

// Local para quadraticas
typedef struct {
	GLUquadric * objecto[256];
	GLUquadric * boneco[256];
} QuadraticasLab;


typedef struct {
	GLint maxLevel;
	GLint actualLevel;
	GLint niveis[LEVEL_MAX_LEVEL][MAP_MAX_X_CELLS][MAP_MAX_Y_CELLS];  // niveis[nivel][celula x][celula y]
} Levels;

typedef struct {
	char statusbar[256] = { " Texto nao iniciado " };
}Texto;

typedef struct {						// INICIADO NO AVANCAR NIVEL... logica.cpp avancarLevel()
	GLboolean apanheiKey1;				// True se jodagor apanhou a chaves.
	GLfloat grauDeApanhoDaChaves;		// Indica escala da chaves, ate desapareces. 1 ate 0
	GLfloat rotacaoChaves;					// Indica escala de rotacao das chaves, 1 ate 0
	GLboolean abertaPorta1;				// True se aberta, e protanto nao desenhamos a porta, nem colisoes
	GLfloat grauDeFechoDaPorta1;		// Grau de abertura da porta. 1 totalmente fechada, 0 totalmente aberta. Usamos para descer a porta.
} Door;


typedef struct {	
	GLuint texturaWall;
	GLuint texturaDoor;
} RepoTextura;

//Definicoes do mundo
typedef struct {
	GLfloat pause;
	OpcoesDesenho opcoesDesenho;
	Vistas vistas;
	Iluminacao iluminacao;
	TeclasLabirinto teclas;
	QuadraticasLab quadraticas;

	GLfloat metro;
	Creatura mainplayer;
	GLint map_cell_max_x;
	GLint map_cell_max_y;
	GLfloat zero_x;
	GLfloat zero_y;
	GLfloat zero_z;
	Cell defaultCell;
	Mapa mapa;
	Levels levels;
	Texto texto;
	Door door;
	RepoTextura texturas;
} Mundo;












// DO PROF.

typedef struct {
	GLboolean   q, a, z, x, up, down, left, right;
} Teclas;



typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLboolean   menuActivo;
	Camera      camera;
	GLboolean   debug;
	GLboolean   ortho;
}Estado;

typedef struct {
	GLfloat       x, y;
	GLint         pontuacao;
}Raquete;

typedef struct {
	GLfloat     x, y;
	GLfloat     velocidade;
	GLfloat     direccao;
	GLfloat     direccaoRodas;
	GLfloat     angTorre;
	GLfloat     angCanhao;
}Tanque;

typedef struct {
	Tanque      tanque;
	GLboolean   parado;
}Modelo;

#endif